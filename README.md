I am working on the current file: [Dissertation](https://gitlab.com/bittern/JB-IIVPSCL1D/-/blob/master/dissertation-tex/dissertation.pdf)

---

Clone this repo:

```
$ git clone https://gitlab.com/bittern/JB-IIVPSCL1D.git
```

If you want to send the repo to a specific directory in your system, just add the path to the end of the command above like so (replace */path/to/directory* with your desired path):

```
$ git clone https://gitlab.com/bittern/JB-IIVPSCL1D.git /path/to/directory
```

Update your local repository (if the repository is not located in the directory *~/JB-IIVPSCL1D* on your machine, replace the path in the following code block with the correct path to the repository):

```
$ cd ~/JB-IIVPSCL1D
$ git pull
```

If you have cloned an older version of this repository and the repository name has changed since then, you can either just delete the repository directory on your machine and `git clone` it, or you can use the following command to change the url your repository is pointing towards for https (if you used ssh then replace the url with the ssh version of the url instead):

```
$ git remote set-url origin https://gitlab.com/bittern/JB-IIVPSCL1D.git
```
-and then `git pull` to check everything went ok.

# Attribution 
Repository icon made by *turkkub* from [Flaticon](www.flaticon.com)
