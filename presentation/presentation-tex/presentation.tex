%%%%%%%%%%%
% Beamer setup %
%%%%%%%%%%%
\documentclass[11pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usetheme{metropolis}
\usefonttheme[onlymath]{serif}
%%%%%%%%%%%
% Environments %
%%%%%%%%%%%
\usepackage{environ} % Provides \NewEnviron{}{}, which enables multiple environments to be defined as a single environment.
% Environment for slides without page numbers.
%\NewEnviron{myframe}[1]{%
%	{\setbeamertemplate{footline}{}
%	\begin{frame}{#1}
%			\BODY
%	\end{frame}}
%}
%%%%%%%%%
% Equations %
%%%%%%%%%
\usepackage{amsmath} % Provides `cases` environment, which gives an extensible left curly-brace (e.g piecewise function), and `subequations` environment.
\usepackage{empheq}
% Left brace containing multiple equations with no label
\NewEnviron{brace*}{%
	\begin{equation*}
		\left\{ \begin{alignedat}{2}
			\BODY
		\end{alignedat} \right.
	\end{equation*}
}
\newcommand{\pdiff}[1]{\partial_{#1}} % Partial differential. In the brackets type the variable you are differentiating with respect to.
\newcommand{\R}{\mathbb{R}} % Real number set.
\newcommand{\x}{\times} % multiplication symbol.
\newcommand{\pf}[1]{\text{#1}} % Function of an explicit parameter.
%%%%%%%
% Figures %
%%%%%%%
\usepackage{tikz}
\usetikzlibrary{arrows.meta} % enables manipulation of line heads. e.g \draw[black, arrows={-stealth, gray}, thick] (0,0) --(0,1); draws a thick black line with a gray arrow head. 
\usetikzlibrary{decorations.pathreplacing} % adds braces in tikz format using \draw [decorate, decoration = {brace}] ...
\usepackage{graphicx} % Insert images using \includegraphics{qr-code}, either in ``figure'' or ``tikzpicture'' environment.
\begin{document}
	\author{Jack Britton \\ {\footnotesize Supervisor: Dr. Omar Lakkis}}
	\title{Investigating the Initial Value Problem for Scalar Conservation Laws in One Dimension}
	%\subtitle{}
	%\logo{}
	%\institute{}
	\date{28 April, 2021}
	%\subject{}
	%\setbeamercovered{transparent}
	%\setbeamertemplate{navigation symbols}{}
	\begin{frame}[plain]
		\maketitle
	\end{frame}
	
	\begin{frame}{The Shallow Water Equations}
		\visible<2>{
		\begin{brace*}{}
			\pdiff{t} h + \pdiff{x} (h \cdot v) & = 0 \\
			\pdiff{t} (h \cdot v) + \pdiff{x} \left(h\cdot v^2 + \frac{h^2}{2}\right)  & = 0.
		\end{brace*}
		}
 	\end{frame}
 
 	\begin{frame}{The Initial Value Problem of Scalar Conservation Laws in One Space Dimension}
 		\visible<1-4>{%
 		\begin{brace*}
 			\color<3>{gray}{\pdiff{t} u + \pdiff{x} F(u) = 0} &\quad 	\color<3>{gray}{\text{in } \R \x (0,\infty)} \\
 			\color<2>{gray}{u = g} &\color<2>{gray}{\quad \text{on } \R \x \{t=0\}.}
 		\end{brace*}
 		}
	 \end{frame}

	\begin{frame}{The Method of Characteristics}
		\centering
		\only<2-5>{%
			\begin{tikzpicture}
				\draw[draw=none, help lines, thick] (-4,-2) grid (4,4);
				\draw[thick] (-4,0) .. controls (0,3) and (3,0) .. (4,4);
				\node at (0,0) {\only<3-5>{\only<3>{$\pf{y}(s) $} \only<4-5>{$\pf{y}(t) = (x, t) $}}};
				\node at (0,-1) {\only<5>{\textit{projected characteristic}}};
			\end{tikzpicture}
		}
		\only<6>{%
			\begin{tikzpicture}
				\draw[draw=none, help lines, thick] (-4,0) grid (4,4);
				% structure
%				\node at (0,-2) {\textit{projected characteristics}};
				\draw[arrows=-to] (-4,0) -- (4.5,0) node[anchor = west] {\small $ x $};
				\draw[arrows=-to] (-4,0) -- (-4,4.5) node[anchor=south] {\small $ t $};
				% contents
				\fill (-2,0) circle (1pt);
				\fill (-1,0) circle (1pt);
				\node at (-2,-0.5) {$ x^0 $};
				\node at (-1,-0.5) {$ \tilde{x}^0 $};
				\node at (2,4) {$ \tilde{\pf{y}}(t) $};
				\node at (4,3) {$ \pf{y}(t) $};
				\draw[thick, arrows=-{latex}] (-2,0) -- (4,4);
				\draw[thick, arrows=-{latex}] (-1,0) -- (3,4);
				\fill[red] (1,2) circle (1pt);
			\end{tikzpicture}
		}
	\end{frame}



	\begin{frame}{A weak solution}
		\visible<+->{%
			\centering
				\begin{tikzpicture}
					\draw[draw=none, help lines, thick] (-1,-1) grid (5,5);
					% region
					\only<+->{%
						\draw (1,0) .. controls (-1,1) and (-1,2) .. (0,3); 
						\draw (0,3) .. controls (1,4) and (2,4) .. (3,3);
						\draw (3,3) .. controls (3,2) and (4,2) .. (5,2);
						\draw (5,2) .. controls (5,0) and (3,0) .. (1,0);
						\node at (2,1.5) {\only<2>{$ V $}};
					}
					\only<+->{%
						\node at (0,2) {$ V_l $};
						\node at (4, 1) {$ V_r $};
					}
					\only<+->{%
					% curve
						\draw[thick] (2,-1) .. controls (0,1) and (2,2) .. (3,4) node[anchor=west] {$ C $};
					}
				\end{tikzpicture}
			}
	\end{frame}

	\begin{frame}{Waves, and Riemann's Problem}
		\centering
		\only<2>{%
			\begin{tikzpicture}[scale=3/4]
				\draw[draw=none, help lines, thick] (-3,-5.5) grid (6,5.5);
				% u = 0
				\draw[arrows=-{latex}] (-2,-4) -- (-2,4);
				\draw[arrows=-{latex}] (-1,-4) -- (-1,4);
				% 0 < u < 1
				\draw[thick, arrows=-{latex}] (0,-4) -- (0,4);
				\draw[thick, arrows=-{latex}] (0,-4) -- (1,4);
				\draw[thick, arrows=-{latex}] (0,-4) -- (2,4);
				\draw[thick, arrows=-{latex}] (0,-4) -- (3,4);
				\draw[thick, arrows=-{latex}] (0,-4) -- (4,4);
				% u = 1
				\draw[arrows=-{latex}] (1,-4) -- (5,4);
				\draw[arrows=-{latex}] (2,-4) -- (5,2);
				\draw[arrows=-{latex}] (3,-4) -- (5,0);
				\draw[arrows=-{latex}] (4,-4) -- (5,-2);
				% structure
				\draw[arrows={-to}] (-3,-4) -- (5.5,-4) node[anchor=west] {$ x $};
				\draw[arrows={-to}] (-3,-4) -- (-3,4.5) node[anchor=south] {$ t $};
				% descriptors
				\draw [decorate, decoration = {brace, mirror}] (-2,-4.5) --  (0,-4.5);
				\node at (-1,-5) {$ u_l = 0 $};	
				\draw [thick, decorate, decoration = {brace}] (0,4.5) --  (4,4.5);
				\node at (2,5) {$ 0 \leq u \leq 1 $};
				\draw [decorate, decoration = {brace, mirror}] (0,-4.5) --  (4,-4.5);
				\node at (2,-5) {$ u_r = 1 $};
				\end{tikzpicture}
			}
		\only<3>{%
			\begin{tikzpicture}[scale=3/4]
				\draw[draw=none, help lines, thick] (-6,-5.5) grid (6.5,5.5);
				% u = 1
				\draw[arrows=-{latex}] (-5,3) -- (-4,4);
				\draw[arrows=-{latex}] (-5,2) -- (-3,4);
				\draw[arrows=-{latex}] (-5,1) -- (-2,4);
				\draw[arrows=-{latex}] (-5,0) -- (-1,4);
				\draw[arrows=-{latex}] (-5,-1) -- (0,4);
				\draw[arrows=-{latex}] (-5,-2) -- (1,4);
				\draw[arrows=-{latex}] (-5,-3) -- (0,2);
				% 1 > u > 0
				\draw[thick, arrows=-{latex}] (-5,-4) -- (-1,0);
				\draw[thick, arrows=-{latex}] (-4,-4) -- (-1,0);
				\draw[thick, arrows=-{latex}] (-3,-4) -- (-1,0);
				\draw[thick, arrows=-{latex}] (-2,-4) -- (-1,0);
				\draw[thick, arrows=-{latex}] (-1,-4) -- (-1,0);
				% u = 0
				\draw[arrows=-{latex}] (0,-4) -- (0,2);
				\draw[arrows=-{latex}] (1,-4) -- (1,4);
				\draw[arrows=-{latex}] (2,-4) -- (2,4);
				\draw[arrows=-{latex}] (3,-4) -- (3,4);
				% shock wave
				\draw[thick] (-1,0) -- (1,4);
				% structure
				\draw[arrows={-to}] (-5,-4) -- (3.5,-4) node[anchor=west] {$ x $};
				\draw[arrows={-to}] (-5,-4) -- (-5,4.5) node[anchor=south] {$ t $};
				% braces
				\draw [decorate, decoration = {brace}] (-5.5,-4) --  (-5.5,3);
				\node at (-6.5,-0.5) {$ u_l = 1 $};
				\draw [thick, decorate, decoration = {brace, mirror}] (-5,-4.5) --  (-1,-4.5);
				\node at (-3,-5) {$ 0 \leq u \leq 1 $};
				\draw [decorate, decoration = {brace, mirror}] (-1,-4.5) --  (3,-4.5);
				\node at (1,-5) {$ u_r = 0 $};				
			\end{tikzpicture}		
		}
	\end{frame}

	\begin{frame}{Applications}
		\centering
		\only<+->{%
		\begin{tikzpicture}
			\draw[draw=none, help lines, thick] (-5,-5) grid (5,4);
			\only<+->{%
				\node at (0,0) {\includegraphics[scale=0.18]{JRA-graphs}};
			}
			\only<+->{%
				\node at (-5,-3.5) {\only<2->{\includegraphics[scale=0.03]{qr-code}}};
				\node at (0,-3.5) {\only<2->{{\tiny gitlab.com/bittern/BotFricTsunami}}};
			}
		\end{tikzpicture}
		}
	\end{frame}
\end{document}