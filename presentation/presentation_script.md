 to Both river and tsunami-induced floods are expected to increase in frequency [1](https://link.springer.com/content/pdf/10.1007%2Fs10584-014-1084-5.pdf) [2](https://advances.sciencemag.org/content/4/8/eaat1180.full), as a result of climate change, and global warming respectively. Understanding the mechanisms of water is imperative to flood preparation and prevention.

































---
Hi my name's Jack, and I was supervised by Dr Omar Lakkis for my project, I'm Investigating the Initial Value Problem for Scalar Conservation Laws in One Dimension

**CHANGE SLIDE: The Shallow Water Equations**

# Motivation

Let me start by providing some motivation.

Last summer, I worked with Omar Lakkis to modify a simulation to show the effects of friction on tsunami waves. By focusing on the programming, I only gained a superficial understanding of the underlying model.

This model was the **Shallow Water Equations**.

**SHOW: Shallow Water Equations**

I'm showing you the one-dimensional version here for simplicity. The first equation describes the **conservation** of mass, and the second describes the **conservation** of momentum.

What we are looking at here is a **system** of **conservation laws**, which together describe the movement of water.

# Aim

To understand these equations, I first had to understand problems involving **conservation laws** in general. Thus, I chose to Investigate the Initial Value Problem for **Scalar** Conservation Laws in **One** Dimension.

# Equation

And this is the problem:

**CHANGE SLIDE: Initial Value Problem of Scalar Conservation Laws in One Space Dimension**

Now with the motivation in mind, lets break it down.

**SHOW: PDE**

The first equation is a **partial differential equation** which describes a **conservation law**, such as one of the Shallow Water Equations. There is only one equation, so we say **scalar**, rather than the **system** we saw earlier.

**SHOW: Initial condition**

The second equation is called the **initial condition**. If we think in terms of simulations, this equation tells us **how** the simulation **starts**. Thinking of the Shallow Water Equations, we are being given the height and velocity of the water at this starting point, that is, we are being given their **initial values**. So this is what makes our problem an **Initial Value Problem**.

**SHOW: Full IVP of scalar conservation law**

So my goal was to define the function ***u*** as some sort of **solution** to the partial differential equation, given an initial condition.

**SLIDE: Method of Characteristics**

# The method of characteristics

Finding a solution for partial differential equations directly is usually quite difficult, especially when compared to ordinary differential equations. So we begin our journey by finding a system of ordinary differential equations, whose solution will help us solve a partial differential equation. How do we do that?

Suppose we know this curve:

**SHOW: CURVE**

Mathematically, we know the function *y* -

**SHOW: y(s)** 

-which maps each point *s* to a point on this curve. Using this knowledge, we can assemble a system of ordinary differential equations, which when solved, can tell us the value of both the function *u* - and its partial differentials at each point on this curve. 

If we swap this proposed curve with the parameters of the function *u* (in our problem, the function *u* has parameters *x*, and *t*, which are space, and time)-

**SHOW: y(t)=*(x*,t)** 

-then we know the value function *u* in its own domain also.

This system is called the characteristic equations, and one of the solutions is the function which described the curve; which we call the projected characteristic.

**SHOW: projected characteristic text**

We say that the function *u* travels along the projected characteristic. For conservation laws, we find the function *u* is constant for all points on a projected characteristic, but **can** have a different value along a different projected characteristic. 

If two projected characteristics intersect-

**SHOW: characteristics crossing**

-it means that the function *u* can have two different values at the intersection point. This means the function *u* is discontinuous, and therefore is also **not** continuously differentiable.

We now look to define a solution which need not be continuously differentiable. This begins our search for a *weak* solution.

**SLIDE: A weak solution**

# A weak solution

Even if the partial derivatives of our function *u* aren't continuous, they must **exist**, otherwise our problem doesn't make sense.

Using this, we can create proxy equation whose solution is also valid for our partial differential equation. We overcome the issue of differential continuity by introducing a function which **is** twice continuously differentiable - called a *test function*. Thus we can transfer the partial differentials **from** *u* onto this **test function** instead.
We call the solution which satisfies this new proxy equation an *integral solution*.

We can split the region-

**SHOW: region**

-that the integral solution acts on into two sides-

**SHOW: left and right sides**

-left and right. Between these two sides runs a curve.

**SHOW: curve** 

What happens when the limit of the integral solution coming from the **left** towards the curve, and from the **right** towards the curve, are different? We'll now discuss what that looks like, and its implications.

**SLIDE: Waves and Riemann**

# Waves and Riemann

To begin this discussion, I'm going to use an example of a scalar conservation law called the invisid Burgers' equation.

The initial value of its integral solution will be described by a piecewise constant function, evaluated at least by the limit from the left of *u*, and from the right.

If the right limit is always larger than the left, the characteristics look like this:

**SHOW: rarefaction waves**

We call the several characteristics lines which fan out from the same point -  a rarefaction wave. In real life, you can think waves in the sea, a sort of expansive motion. 

If instead, the left limit is larger than the right, we have this:

**SHOW: shock waves**

The characteristics between the left and right limits converge to one point, forming of a shock wave. An example of this is the movement of water at the front of a moving ship.

**Any** scalar conservation law combined with **either** of the initial conditions we mentioned is called a *Riemann Problem*.

It turns out we **can** formulate a solution which satisfies this problem. We call it an *entropy solution*, and discover its also **unique**.

The **key issue** in solving **our** problem was that it was possible for characteristics to **intersect**, which introduced the possibility of **discontinuities** for the function *u*. Formulating a **unique entropy solution** to the **Riemann problem**, means we formulate a **unique weak solution** for **our** Initial value problem - in the form of the function *u*.

**SLIDE: applications**

# Applications

The concepts we've covered here have been extended to systems of conservation laws, including the Shallow Water Equations in one dimension, often used to model rivers.

Then with further work, we may reach the multi-dimensional systems of conservation laws.

**SHOW: Tsunami graphs**

Here are some of the results from my simulation **based on** the multi dimensional Shallow Water Equations, just to show the complexity that can be achieved building on the work we have done here. 

# End slide

**SLIDE: end slide**

If you want to see more information about the simulation, you can either-

**SHOW: QR code**

-scan the QR code or head to my gitlab page.

**SHOW: Thank you**

*Pause*

Thank you **very much** for your time. If you have **any** questions, please ask away.



# General overall method
## Method of characteristics
1. Define characteristic equations
2. The initial condition is defined on a boundary -> use the characteristic equations to solve the IVP at a point on the boundary
3. expand to neighbourhood around that point
4. show that a solution exists for a twice continuously differentiable U.

## Conservation Laws analysis
1. Define integral solution
2. Split into two domains -> find a balance law called the Rankine-Hugoniot condition which says the difference between the left and right limit of the function *u* multiplied by the speed of the curve is equal to the difference between the function *F* evaluated at the same left and right limit of u.
3. Move backwards along the projected characteristic, cause forward we have a discontinuity. Find a inequality called the entropy condition, saying the differential of the function *F* evaluated at the left limit of *u* is larger than the speed of the curve, which is larger than *F* at the right limit. 
4. Define the Lax-Oleinik formula, which defines *u* as an integral solution.
5. Find the entropy condition for the Lax-Oleinik defined solution.
6. Find that entropy solutions are unique almost everywhere.
7. Define Riemann's problem as the initial value piecewise constant evaluated at *u* left and *u* right. 
8. Define the solution to the unique entropy solution to the Riemann problem for 2 possible initial states of *u* (left bigger than right or vice versa).