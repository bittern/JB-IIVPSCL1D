\begin{appendices}
	
	\section{Notation}
	\begin{description}[font=\normalfont, leftmargin=1cm, style=nextline]
		\item[$ \mx{x} = \vect{x_1, \ldots, x_n} $ / \scshape{bold font letter equals square brackets around several letters}]
		A matrix if the elements are an $ n \x n $ count, otherwise it is either a row or column vector dependent on the context. For example, it two vectors are being multiplied such as $ \mx{a} \cdot \mx{b} $, for $ \mx{a}_{n \x 1} $ and $ \mx{b}_{n \x 1} $, it safe to assume this is really $ \mx{a}^T \cdot \mx{b} $.
		
		\item[ $ \tuple{a, b, c} $ / \scshape{isolated round brackets around several letters} ]
		A tuple, usually a collection of terms which are not necessarily denoted by a single letter (as a matrix/ vector is).
		
		\item[$ x_i $ / \scshape{subscript}]
		The $ i $-th element of a matrix, so that $ \mx{x} = \vect{x_1, \ldots, x_n} $.
		
		\item[$ \tilde{x} $ / \scshape{letter with tilde above}]
		The default notation for a specific point $ \tilde{x} $ in the set of points $ x $.
		
		\item[$ x^0 $ / \scshape{letter superscript zero}]
		Usually denotes a point - or a function of a point - on a boundary.
				
		\item[$ \pf{p}(s) $ / \scshape{non-itallic symbol}]
		A function of some unknown parameter $ s $.
		
		\item [$ \pdiff{x} f $ / \scshape{partial symbol with subscript}]
		The partial differential of the function $ f $ with respect to $ x $. That is, $ \pdiff{x} f = \frac{\partial f}{\partial x} $.
		
		\item[$ \pdiff{x_i} f $ / \scshape{partial symbol with subscript, with a subscript}]
		The partial differential of the function $ f $ with respect to $ x_i $, where $ \mx{x} = \vect{x_1, \ldots, x_n} $. That is, $ \pdiff{x_i} f = \frac{\partial f}{\partial x_i} $.
		
		\item[$ \pdiff{y} \pdiff{x} f $ / \scshape{two partial symbols with subscripts}]
		The second order partial differential of the function $ f $, first with respect to $ x $, then with respect to $ y $. That is, $ \pdiff{y} \pdiff{x}  f  = \frac{\partial}{\partial y} \left( \frac{\partial f}{\partial x} \right) $.
		
		\item[$ Du $ / \scshape{capital D next to a letter}]
		The gradient of $ u $. For the function $ u(\mx{x}) $, we may define $ Du $ as:
		\begin{equation*}
			Du = \vect{\pdiff{x_1} u, \ldots, \pdiff{x_n} u}.
		\end{equation*}
		
		\item[$ D_{\mx{y}} F $ / \scshape{capital D subscript}]
		The gradient of $ F $ along $ \mx{y} $. For the function $ F(\mx{x}, \mx{y}) $, we may define $ D_{\mx{y}} F $ as:
		\begin{equation*}
			D_{\mx{y}} F = \vect{\pdiff{y_1} F, \ldots, \pdiff{y_n} F}.
		\end{equation*}
	
		\item[$ \dot{f} $ / \scshape{dot on top of a letter}]
		The derivative of the single variable function $ f(x) $. That is, $ \dot{f} = \frac{df}{dx} $.
				
		\item[$ u \in C(U; \R) $ / \scshape{semi-colon in brackets}]
		The function $ u $ maps $ U $ to $ \R $, and $ u $ is continuous on $ U $. i.e., $ \tfns{u}{U}{\R} $, and $ u \in C(U) $.
		
		\item[$ u \in C^k (U) $ / \scshape{capital C superscript}]
		The function $ u $ is $ k $ times continuously differentiable on the set $ U $.
		
		\item[$  u \in C^\infty (U) $ / \scshape{capital C superscript infinity}]
		The function $ u $ is infinitely continuously differentiable on the set $ U $. We equally say $ u $ is \textit{smooth} on $ U $.
		
		\item[$ \delta_{ij} $/ \scshape{delta subscript}]
		The Kronecker delta, which is the function:
		\begin{equation*}
			\delta_{ij} = \begin{cases}
				1 \mcomma & \quad \text{for } i = j \\
				0 \mcomma & \quad \text{for } i \neq j.
			\end{cases}
		\end{equation*}
		\forcebreak
		\item[$ \nb{=}{\ast} $/ \scshape{equal sign with a symbol above it}]
		An equality which is explicitly explained either before or after the equation.
		
		\item[$ B(x^0, r) $ / \scshape{capital B next to pair of brackets}]
		A ball $ B $ with centre $ x^0 $, and radius $ r $. 
		
		\item[$ \bar{U} $ / \scshape{bar over a capital letter}]
		The set $ \bar{U} $ is the closure of the set $ U $, where $ U $ may be an open set.
		
		\item[$ \partial U \text{ \normalfont{is} } C^k $ / \scshape{partial symbol next to capital letter is capital C superscript}]
		$ \partial U $ is $ C^k $ is defined in  \cref{apx:boundary-ck}.
		
		\item[$ \nu $ / \scshape{nu}]
		$ \nu $ denotes the outward unit normal, defined in  \entref{apx:unit-norm-vec-field}.
		
		\item[\underline{special $ x $} / \scshape{underline}]
		The core item being defined in a definition, where using italics to make the item stand out is not possible (e.g in an equation, where maths is typically italicised anyway).
		
		\item[$ F' $ / \scshape{letter with an apostrophe}]
		The differentiated function of $ F $.
		
		\item [$ L^{\infty} $ / \scshape{capital L superscript infinity}]
		$ L^{\infty} $ is an $ L^p $ space, for which $ p = \infty $.
		
		\item[ $ L $ /  \scshape{capital L}]
		$ L $ represents the Lagrangian, defined in \entref{apx:lagrangian}.
		
		\item [$ \R^n $ / \scshape{blackboard font letter superscript letter}]
		The set $ \R $ is $ n $ dimensional, where $ n \in \N $.
		
		\item [$ \exp(x) $ / \scshape{exp bracket terms}]
		The exponential operator, also known as $ e^{x} $.
		
		\item[$ \vert $ / \scshape{vertical line}]
		The symbol for the phrase ``such that''. This phrase may be referred to in other works as the $ : $  symbol. This notation only applies in a mathematical environment. 
	\end{description}
	\newpage
	
	\section{Theorems, definitions, and methods} \label{sec:apx-thms}
	\begin{theorem}[Clairaut's theorem]\label{apx:clairaut-thm}
		Let $ U $ be an open subset of $ \R^n $, let $ \tfns{f}{U}{\R^m} $ be a twice continuously differentiable function, and let  $ \tilde{\mx{x}} = \vect{\tilde{x}_{1}, \ldots, \tilde{x}_{n}} $ be a point in $ U $. %\\
		Then: 
		\begin{equation*}
			\pdiff{x_i} \pdiff{x_j} f(\tilde{\mx{x}}) = \pdiff{x_j} \pdiff{x_i} f(\tilde{\mx{x}}),
		\end{equation*}
		for all $ 1 \leq i,j \leq n $, where $ n \in \N $.
	\end{theorem}
	\interp{TaoAI2016}{\S 6.5 $ \vert $ Theorem 6.5.4}\\
	
	\begin{definition}\label{apx:boundary-ck}
		Let the set $ U \subset \R^n $ be open and bounded, and $ k \in \N $. Then, the boundary \underline{$ \partial U $ is $ C^k $} if - for each point $ \mx{x}^0 \in \partial U $ - there exists an $ r > 0 $ and a function $ \gamma \in C^k( \R^{n-1} ; \R ) $ such that:
		\begin{equation*}
			U \cap B(\mx{x}^0 , r) = \Set{\mx{x} \in B(\mx{x}^0 , r) | x_n > \gamma (x_1, \ldots, x_{n-1})}.
		\end{equation*}
	\end{definition}
	\interp{EvansPDE2010}{\S C.1. $ \vert $ Definition}\\
	
	\forcebreak
	\begin{method}[Straightening the boundary] \label{apx:straight-bound}
		Let $ U \subset \R^n $ be open and bounded, and $ k \in \N $. Suppose the boundary $ \partial U $ is $ C^k $ (see \cref{apx:boundary-ck}), fix $ \mx{x}^0 \in \partial U $, and take $ \gamma $ as in \cref{apx:boundary-ck}. Now set:
		\begin{brace*}
			y &= \mx{\Phi} (\mx{x}) \\
			x &= \mx{\Psi} (\mx{y}),
		\end{brace*}
		where:
		\begin{brace*}
			y_i &= x_i \eqqcolon \Phi_{i} (\mx{x}), &\eqfor i = 1, \ldots, n-1 \\
			y_n &= x_n - \gamma(x_1, \ldots, x_{n-1}) \eqqcolon \Phi_{n} (\mx{x}), &
		\end{brace*}
		and:
		\begin{brace*}
			x_i &= y_i \eqqcolon \Psi_{i} (\mx{y}), &\eqfor i = 1, \ldots, n-1 \\
			x_n &= y_n + \gamma(y_1, \ldots, y_{n-1}) \eqqcolon \Psi_{n} (\mx{y}). &
		\end{brace*}
	Then the equality $ \mx{\Phi} = \mx{\Psi}^{-1} $ holds, and we say that the mapping $ \map{\mx{x}}{\mx{\Phi}(\mx{x}) = \mx{y}} $ \dex{straightens the boundary} $ \partial U $ near $ \mx{x}^0 $.
	\end{method}
	\interp{EvansPDE2010}{\S C.1. $ \vert $ Straightening the boundary}\\
	\forcebreak
	\begin{theorem}[Implicit function theorem] \label{apx:imp-fun-thm}
		Let $ U $ be an open subset of $ R^n $, let $ \tfns{f}{U}{\R} $ be continuously differentiable, and let $ \mx{y} = \vect{y_1, \ldots, y_n} $ be a point in $ U $ such that $ f(\mx{y}) = 0 $ and $ \pdiff{x_n} f(\mx{y}) \neq 0$.
		Then there exists an open subset $ V $ of $ \R^{n-1} $ containing  $ \vect{y_1, \ldots, y_{n-1}} $, an open subset $ W $ of $ U $ containing $ \mx{y} $, a function $ \tfns{g}{V}{\R} $ such that $ g(y_1, \ldots, y_{n-1}) = y_n $, and for $ \mx{x} = \vect{x_1, \ldots, x_n}$ the following holds:
		\begin{gather*}
			\Set{ \mx{x} \in W | f(\mx{x}) = 0 } \\
			= \Set{ \vect{x_1, \ldots, x_{n-1}, g(x_1, \ldots, x_{n-1})} | \vect{x_1, \ldots, x_{n-1}} \in V }.
		\end{gather*}
		Furthermore, $ g $ is differentiable at $ \vect{y_1, \ldots, y_{n-1}} $, and:
		\begin{equation*}
			\pdiff{x_j} g(y_1, \ldots, y_{n-1}) = - \frac{\pdiff{x_j} f(\mx{y})}{\pdiff{x_n} f(\mx{y})},
		\end{equation*}
		for all $ 1 \leq j \leq n - 1 $, where $ n \in \N $.
	\end{theorem}
	\interp{TaoAI2016}{\S 6.8 $ \vert $ Theorem 6.8.1}\\
	\forcebreak
	\begin{theorem}[Inverse function theorem] \label{apx:inv-fun-thm}
		Let $ U \subset \R^n $ be an open set, $ \tfns{\mx{f}}{U}{\R^n} $ be continuously differentiable, and:
		\begin{equation*}
			\det D \mx{f} (\tilde{\mx{x}}) \neq 0.
		\end{equation*}
		Then there exists an open subset $ V \subset U $, with $ \tilde{\mx{x}} \in V $, and an open subset $ W \subset \R^n $, with $ \mx{f}(\tilde{\mx{x}}) \in W $, such that:
		\begin{enumerate}[label=(\roman*)]
			\item The function:
			\begin{equation*}
				\efns{\mx{f}}{V}{W},
			\end{equation*}
			is bijective.
			
			\item The inverse function:
			\begin{equation*}
				\efns{\mx{f}^{-1}}{W}{V},
			\end{equation*}
			is continuously differentiable.
			
			\item If $ \mx{f} $ is $ k $ times continuously differentiable, then so is $ \mx{f}^{-1} $, for $ k \in \N \mcomma k \geq 2 $.
		\end{enumerate}
	\end{theorem}
	\interp{EvansPDE2010}{\S C.6. $ \vert $ Theorem 8} \\
	\forcebreak
	\begin{method}[Integrating factor method] \label{apx:int-fct-mth}
		The following is a method to solve first order, linear ODEs.\\
		For such an ODE with form:
		\begin{equation} \label{eq:1linearODE}
			\frac{dx}{dt} + f(t)x = g(t),
		\end{equation}
		where $ f $ and $ g $ are continuous functions of $ t $; a solution can be obtained through the utilising the integrating factor:
		\begin{equation*}
			I(t) = \exp \left( \int f(t) \intspace dt \right).
		\end{equation*} 
		Through multiplying \eqref{eq:1linearODE} by $ I(t) $, we receive:
		\begin{equation*}
			\frac{d}{dt} \Big( I(t) \cdot x(t) \Big) = I(t) \cdot g(t).
		\end{equation*}
		This equation can then be integrated and solved for $ x(t) $ to find the solution in its general form:
		\begin{equation*}
			x(t) = \frac{\int I(t) \cdot g(t) \intspace dt}{I(t)}.
		\end{equation*}
		Using the initial conditions (if given) can subsequently reveal the specific solution.\\
	\end{method}
	\interp{SchroersODE2011}{\S 1.2.4}\\	
	
	\begin{theorem}[Differentiability implies continuity]\label{apx:diff-imp-cont}
		Let the function $ f $ be differentiable at the point $ \tilde{x} $. Then $ f $ is continuous at $ \tilde{x} $ also.
	\end{theorem}
	\interp{JerisonMOM2010}{1. Differentiation $ \vert $ Part A: Definition and Basic Rules $ \vert $ Session 5: Discontinuity $ \vert $ Section E}\\
	\forcebreak
	\begin{definition}[Unit normal vector field] \label{apx:unit-norm-vec-field}
		Let the set $ U \subset \R^n $ be open and bounded. If $ \partial U $ is $ C^1 $ (see \cref{apx:boundary-ck}), then the outward pointing unit normal vector field along $ \partial U $ is denoted:
		\begin{equation*}
			\mx{\nu} = \vect{\nu_1, \ldots, \nu_n}.
		\end{equation*}
	\end{definition}
	\interp{EvansPDE2010}{\S C.1. $ \vert $ Definitions $ \vert $ (i)} \\
	
	\begin{theorem}[Multi dimensional integration by parts formula] \label{apx:mlt-dim-int-parts}
		Let $ u \mcomma v \in C^1(\bar{U}) $, where $ U $ is a bounded, open subset of $ \R^n $, and the boundary $ \partial U $ is $ C^1 $. Then:
		\begin{equation*}
			\int_{U} \pdiff{x_i} u \cdot v \intspace dx = \int_{\partial U} u \cdot v \cdot \nu_i \intspace dS - \int_{U} u \cdot \pdiff{x_i} v \intspace dx,
		\end{equation*}
	for $ i = 1, \ldots, n $, where $ \nu_i $ is a unit normal vector.
	\end{theorem}
	\interp{EvansPDE2010}{\S C.2. $ \vert $ Theorem 2} \\
	
	\begin{method}[Unit normal vector field calculation] \label{apx:calc-unit-norm-vfield}
		Suppose $ C $ is a two dimensional smooth curve described parametrically by the vector valued function $ \mx{y} = \vect{t, x(t)} $. Let $ \mx{\tau} $ denote the tangent vector field, and let $ \mx{\nu} $ be the unit normal vector field. Then we have the following definitions:
		\begin{brace*}
			\mx{\tau} &= \frac{d}{dt} (\vect{t, x(t)}) \\
			\mx{\nu} &= \vect{-\tau_2, \tau_1} \cdot \norm{\mx{\tau}}^{-1}.
		\end{brace*}
	\end{method}
	Adapted from \cite{Cau} - Integrating multivariable functions $ \vert $ Line integrals in vector fields. \\
	\forcebreak
	\begin{definition}[The Lagrangian] \label{apx:lagrangian}
		Let the function $ \tfns{L}{\R^n}{\R} $ be a given smooth function named the \dex{Lagrangian}. 
	\end{definition}
	\interp{EvansPDE2010}{\S 3.3.1 $ \vert $ The calculus of variations}\\
	
	\begin{definition}[Legendre transform] \label{apx:leg-trans}
		The \dex{Legendre transform} of a function $ L $ is:
		\begin{equation*}
			L^{\ast} (\mx{v}) \coloneqq \sup_{\mx{p} \in \R^n} \Set{\mx{v} \cdot \mx{p} - L(\mx{p}) }, \eqfor \mx{v} \in \R^n.
		\end{equation*}
		The definition is the same when using the maximum, rather than the supremum.
	\end{definition}
	\interp{EvansPDE2010}{\S 3.3.2 $ \vert $ Definition, and \S 3.3.2 $ \vert $ Motivation for Legendre Transform}\\
	
	\begin{definition}[IVP for the Hamilton-Jacobi equation] \label{apx:ham-jac-eq}
		The IVP for the \dex{Hamilton-Jacobi equation} is:
		\begin{brace*}
			\pdiff{t} u + H(Du) &= 0 \quad &&\text{in } \R \x (0,\infty) \\
			u &= g  \quad &&\text{on } \R \x \{t=0\},
		\end{brace*}
		where $ \tfns{u = u(\mx{x}, t)}{[0,\infty)}{\R}  $ is unknown; and both the \dex{Hamiltonian} $ \tfns{H}{\R^n}{\R}$, and the initial function $ \tfns{g}{\R^n}{\R} $, are given.
	\end{definition}
	\interp{EvansPDE2010}{\S 3.3 $ \vert $ Equation 1}\\
	\forcebreak
	\begin{theorem}[Hopf-Lax formula] \label{apx:hopf-lax-form}
		Suppose $ \mx{x} \in \R^n $, $ s > 0 $, and $ t > 0 $. Also suppose the Lagrangian is a convex function $ \tfns{L}{\R^n \x \R^n}{\R} $ which satisfies the conditions:
		\begin{equation*}
			\lim_{|v| \rightarrow \infty} \frac{L(v)}{|v|} = + \infty,
		\end{equation*}
		and:
		\begin{brace*}
			L &= H^{\ast} \\
			H &= L^{\ast},
		\end{brace*}
		where $ H $ is the Hamiltonian, and * represents the Legendre Transform.
		Then the solution of the minimisation problem:
		\begin{equation*}
			\inf_{\mx{w}(\cdot) \in C^1} \Set{\int_{p}^{t} L(\dot{\mpf{w}}(s)) \intspace ds + g(\mpf{w}(0))  | \mpf{w}(t) = \mx{x}},
		\end{equation*}
		is:
		\begin{equation*}
			u(\mx{x}, t) = \min_{\mx{y} \in \R} \Set{tL \left( \frac{\mx{x} - \mx{y}}{t} \right) + g(\mx{y})},
		\end{equation*}
		where $ g(\mx{x}) = u(\mx{x}, 0) $.
	\end{theorem}
	\interp{EvansPDE2010}{\S 3.3.2 $ \vert $ Theorem 4}\\
	\forcebreak
	\begin{theorem}[Unique, weak solution of the Hamilton-Jacobi equation] \label{apx:sol-ham-jac-eq}
		Suppose $ H \in C^2 $, is convex and:
		\begin{equation*}
			\lim_{|p| \rightarrow \infty} \frac{H(p)}{|p|} = + \infty.
		\end{equation*} 
		Also suppose $ \tfns{g}{\R^n}{\R} $ is Lipschitz continuous. Furthermore, suppose either $ g $ is semiconcave or $ H $ is uniformly convex, and that $ u $ defined by the Hopf-Lax formula (see \cref{apx:hopf-lax-form}):
		\begin{equation*}
			u(\mx{x}, t) = \min_{\mx{y} \in \R^n} \Set{tL \left( \frac{\mx{x} - \mx{y} }{t} \right) + g(\mx{y})},
		\end{equation*}
		is differentiable at a point $ (\mx{x}, t) \in \R^n \x (0,\infty) $. Then $ u $ is the unique, weak solution of the IVP for the Hamilton-Jacobi equation (see \cref{apx:ham-jac-eq}):
		\begin{brace*}
			\pdiff{t} u + H(Du) &= 0 \quad &&\text{in } \R \x (0,\infty) \\
			u &= g \quad &&\text{on } \R \x \{t=0\}.
		\end{brace*}
	\end{theorem}
	\interp{EvansPDE2010}{\S 3.3.2 $ \vert $ Theorem 5, and \S 3.3.3 $ \vert $ Theorem 8}\\
	
	\begin{theorem}[Rademacher's theorem] \label{apx:radem-thm}
		Let the function $ u $ be locally Lipschitz continuous in $ U $. Then $ u $ is differentiable almost everywhere in $ U $.
	\end{theorem}
	\interp{EvansPDE2010}{\S 5.8.3 $ \vert $ Theorem 6}\\
	\forcebreak
	\begin{definition}[Lipschitz constant] \label{apx:lip-const}
		Let $ \tfns{f}{\R^n}{\R} $ be a Lipschitz continuous function. Then the \dex{Lipschitz constant} is defined as:
		\begin{equation*}
			\text{Lip}(f) = \sup_{x,y \in \R^n} \Set{\frac{\lvert f(x) - f(y) \rvert}{\lvert x - y \rvert}} < \infty,
		\end{equation*}
		where $ x \neq y $.
	\end{definition}
	\interp{EvansPDE2010}{\S 3.3.2 $ \vert $ Equation 20}\\
\end{appendices}