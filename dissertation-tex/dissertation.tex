%%%%%%%%%%%
% Document info%
%%%%%%%%%%%
\documentclass[12pt,a4paper]{article}
%%%%%%%%%
%  Title Page %
%%%%%%%%%
\usepackage{disstyle} % This is quite pointless, but makes for a good reference in the future if I find a need/ a LaTeX editor which can suggest from a .sty.
% The following preamble can go in a .sty, however TeXStudio will not suggest and autocomplete for the commands and citations defined here.
%%%%%%%%
% Packages %
%%%%%%%%
%% Other %%
\usepackage[utf8]{inputenc} % 1. Control numbering of theorems-like items. 2. Dependency for biblatex to print citations.
\usepackage[english]{babel} % 1. Control numbering of theorems-like items. 2. Dependency for biblatex American English(?)
\usepackage[normalem]{ulem} % Adds strikeout (\sout{}). `normalem`  leaves the command \emph{} unchanged.
\usepackage{xcolor} % coloured text.
\usepackage{enumitem} % `Description` environment tweaks. Also enables lists with roman numerals using \begin{proofsteps}[label=(\roman*)] (so list is (i), (ii), ...).
\usepackage[page, title, titletoc]{appendix} % Appendix formatting. `page` titles the appendix section with ``Appendices''. `title` prepends each section in the appendix with ``Appendix''. `toc` adds the title ``Appendices'' to the table of contents. `titletoc` prepends each section (of the appendix) in the table of contents with ``Appendix''.
\usepackage{chngcntr} % provides \counterwithin{}{} which resets the equation counter for each section
\usepackage{tikz} % provides manufacturing of graphics, in the `tikzpicture` environment. Use \draw[help lines, thick] (0,0) grid (4,4); as a gridline to help with positioning.
\usetikzlibrary{arrows.meta} % enables manipulation of line heads. e.g \draw[black, arrows={-stealth, gray}, thick] (0,0) --(0,1); draws a thick black line with a gray arrow head. 
\usetikzlibrary{decorations.pathreplacing} % adds braces in tikz format using \draw [decorate, decoration = {brace}] ...
\usepackage{imakeidx} % package to make index. For hyperlinks to work in index, call `imakeidx` before `hyperref`.
\usepackage[hidelinks, hypertexnames=false]{hyperref} % Hyperlinks. `hidelinks` removes red border around hyperlinks. For hyperlinks to work in index, call `imakeidx` before `hyperref`. `naturalnames=true` distinguishes references to e.g, equation 1b), and equation 2) as ...1b and ...2 in the link, where otherwise they would both point to the same equation 1b) without this flag; though if there is another equation in a different section with the same number, it will point to the first occurence of the equation only (so \counterwithin*{}{} is not usable). `hypertexnames=false` names every link individually for an environment, so e.g equation 1b), and equation 2) have ...2 and ...3 in the link where otherwise they would both point to the same equation 1b) without this flag; though there could be issues with the index.
%% Bibliography %%
\usepackage{biblatex} %bibliography manager.
\usepackage{csquotes} % dependency for babel (-> biblatex)
\usepackage{url} % url in citations
%% Maths %%
\usepackage{empheq} % Provides `empheq` environment, which enables each equation in a left curly brace to be labelled individually, when inside the `subequations` environment.
\usepackage{amsmath} % Provides `cases` environment, which gives an extensible left curly-brace (e.g piecewise function), and `subequations` environment.
\usepackage{environ} % Provides \NewEnviron{}{}, which enables multiple environments to be defined as a single environment.
%%% Symbols %%%
\usepackage{mathtools} % \coloneq (:=) symbol .
\usepackage{braket} % Handles set notation with \Set{}, including pipe | and bracket { } resizing
\usepackage{accents} % Provides \accentset{symbol1}{symbol2}, to put symbol1 above symbol2. e.g for an asterisk above an equality sign, do: \accentset{\ast}{=}.
%%% Fonts %%%
\usepackage{amssymb} % Font for number sets (\mathbb{})
\usepackage{bm} % A bold math font.
%%% Theorems %%%
\usepackage[capitalise]{cleveref} % Referencing for theorem-like environments using \cref{}. `capitalize`  capitalises the reference environment (like ``Lemma'' rather than ``lemma'')
\usepackage{thmtools} % Tools to make theorem-like environments. Also allows the use of \nameref{} to cite theorems by their names, useful for appendix theorems (\nameref{} doesn't play well with other theorem packages I've tried like `ntheorem`). \cref{} can be used to cite theorems as ``theorem 2'' for example. Used with this package (as opposed to others like `ntheorem`), it also respects different theorem-like environments, so using \cref{} with a lemma environment will show in the text as ``Lemma 5'' for example.
%%%%%%%%%%
% Bibliography %
%%%%%%%%%%
\addbibresource{dissertation.bib} % add bibliography file from this location (relative to the tex file's path.)
%%%%%%%%%
% Commands %
%%%%%%%%%
%% Symbols %%
\newcommand{\pdiff}[1]{\partial_{#1}} % Partial differential. In the brackets type the variable you are differentiating with respect to.
\newcommand{\R}{\mathbb{R}} % Real number set.
\newcommand{\N}{\mathbb{N}} % Natural number set.
\newcommand{\x}{\times} % multiplication symbol.
\newcommand{\mcomma}{, \,} % comma space to be used in math mode. so $ a \mcomma b $ is the same as $ a $, $ b $.
\newcommand{\odiv}{\text{div}} % The divergence operator.
%% Frequent groups of symbols %%
\newcommand{\tfns}[3]{#1 \colon \, #2 \rightarrow #3}  % Function set mapping (in-text). It should display as follows: 	#1:	 #2 -> #3.
\newcommand{\map}[2]{#1 \mapsto #2}  % Function set mapping (in-text). It should display as follows: 	#1 |-> #2.
\newcommand{\efns}[3]{#1 \colon \quad #2 \longrightarrow #3} % Function set mapping (in equation environment).  It should display as follows: 	#1:		 #2		--->	 #3.
\newcommand{\nb}[2]{\; \accentset{#2}{#1} \;} % Notable equality, an 1. equation symbol (e.g =) with 2. a symbol (e.g *) above it to easily identify and discuss.
\newcommand{\pozoxo}{ \mx{p}^0, z^0, \mx{x}^0 } % The triple of p, z, x, with the parameter s equal to 0.
\newcommand{\qgy}{ (\mx{q}(\mx{y}), g(\mx{y}), \mx{y}) } % The triple of (q, g, y).
\newcommand{\pszsxs}{ (\mpf{p}(s), \pf{z}(s), \mpf{x}(s)) } % The triple of (p, z, x) with parameter s.
\newcommand{\Duux}{ (Du, u, \mx{x}) } % The triple (Du, u, x).
\newcommand{\eqfor}{\quad \text{for }} % within an equation environment, stating that an equation holds under certain conditions
\newcommand{\intspace}{\enspace} % space between integrand and integrating variable (i.e \int u dx, the space between u and dx).
\newcommand{\dintspace}[2]{\enspace #1 \, #2} % space between integrand and two integrating variables (i.e \int u dx dt, the space between u and dx, dx and dt).
\newcommand{\norm}[1]{\left\lVert #1 \right\rVert} % The norm of #1.
\newcommand{\vect}[1]{\left[#1\right]} % Create a column vector.
\newcommand{\tuple}[1]{\left(#1\right)} % Create a tuple.
%% Fonts %%
\newcommand{\pf}[1]{\text{#1}} % Function of an explicit parameter.
\newcommand{\mx}[1]{\bm{#1}} % Matrix font.
\newcommand{\mpf}[1]{\textbf{#1}} % Matrix of explicit parameter functions.
%% Editting %%
\newcommand{\toedit}[1]{\textcolor{green}{#1}} % Text to be editted.
\newcommand{\editsectionline}{\toedit{\hrule} \vspace{5mm}} % Line to indicate a section which should be editted.
\newcommand{\comment}[1]{\textcolor{red}{#1}} % Comments text colour.
%% Other %%
\newcommand{\paranewline}[1]{\paragraph{#1}\mbox{}\\} % \paranewline{title} creates a new paragraph, with the "title" in bold, and the subequent unformatted text is written on a newline, rather than on the same line as the title.
\newcommand{\interp}[2]{Adapted from \citeauthor{#1} \cite{#1} - #2.} % Sentence to be added after a theorem in the appendix. #1 is the citation key; #2 is the location in the document where the theorem is written.
\newcommand{\entref}[1]{\cref{#1} (\nameref{#1})} % Refer to Environment-Number-Title of a label.
\newcommand{\dex}[1]{\textit{#1}\index{#1}} % An new important word, which is given italic font and put in the index.
\newcommand{\stpprfref}[2]{\ref{#1} of the proof of \entref{#2}} % Refer to step #1 of the proof of a theorem-like statement #2.
\newcommand{\authnumcite}[1]{\citeauthor{#1} \cite{#1}} % Default author citation layout
\newcommand{\forcebreak}{\clearpage} % Command to break a page, to be changed on all occurences if neccessary throughout the document if a block has been significantly changed
%%%%%%%%%
% Theorems %
%%%%%%%%%
\declaretheorem[thmbox={style=S, bodystyle=\normalfont}, numberwithin=section, name=THEOREM]{theorem} % `thmbox` creates a box around the theorem of a certain `style`. `bodystyle` is the font style used within the environment. The default is itallic. `numberwithin` is the numbering method used for the environment (in this case, ``section'' will refresh the count for each new section called into).
\crefname{theorem}{Theorem}{Theorems} % let cleveref (\cref{}) refer to a label on the theorem environment as ``Therorem'' for 1 reference, or ``Theorems'' for several.
\declaretheorem[sibling=theorem, thmbox={style=S, bodystyle=\normalfont}, name=LEMMA]{lemma} % `sibling` ties the counter to the given theorem-like environment (in this case ``lemma'').
\crefname{lemma}{Lemma}{Lemmas} % let cleveref (\cref{}) refer to a label on the lemma environment as ``Lemma'' for 1 reference, or ``Lemmas'' for several.
\declaretheorem[sibling=theorem, thmbox={style=S, bodystyle=\normalfont}, name=METHOD]{method} % `sibling` ties the counter to the given theorem-like environment (in this case ``method'').
\crefname{method}{Method}{Methods} % let cleveref (\cref{}) refer to a label on the method environment as ``Method'' for 1 reference, or ``Methods'' for several.
\declaretheorem[sibling=theorem, thmbox={style=S, bodystyle=\normalfont}, name=DEFINITION]{definition} % `sibling` ties the counter to the given theorem-like environment (in this case ``definition'').
\crefname{definition}{Definition}{Definitions}% let cleveref (\cref{}) refer to a label on the definition environment as ``Definition' for 1 reference, or ``Definitions'' for several.
\declaretheorem[sibling=theorem, thmbox={style=S, bodystyle=\normalfont}, name=NOTATION]{notation} % `sibling` ties the counter to the given theorem-like environment (in this case ``notation'').
\crefname{notation}{Notation}{Notations} % let cleveref (\cref{}) refer to a label on the notaion environment as ``Notation'' for 1 reference, or ``Notations'' for several.


% IF NECCESSARY TO AVOID PAGE BREAKS AT ALL COSTS
%%%%%%%%%%%%
%\newenvironment{absolutelynopagebreak}
%{\par\nobreak\vfil\penalty0\vfilneg
%	\vtop\bgroup}
%{\par\xdef\tpd{\the\prevdepth}\egroup
%	\prevdepth=\tpd}

%\declaretheorem[thmbox={style=S, bodystyle=\normalfont}, numberwithin=section, name=THEOREM]{Theorem} % `thmbox` creates a box around the theorem of a certain `style`. `bodystyle` is the font style used within the environment. The default is itallic. `numberwithin` is the numbering method used for the environment (in this case, ``section'' will refresh the count for each new section called into).
%\crefname{Theorem}{Theorem}{Theorems} % let cleveref (\cref{}) refer to a label on the theorem environment as ``Therorem'' for 1 reference, or ``Theorems'' for several.
%\newenvironment{theorem}
%	{\begin{absolutelynopagebreak}
%		\begin{Theorem}}
%	{\end{Theorem}
%	\end{absolutelynopagebreak}}
%
%\declaretheorem[sibling=Theorem, thmbox={style=S, bodystyle=\normalfont}, name=LEMMA]{Lemma} % `sibling` ties the counter to the given theorem-like environment (in this case ``lemma'').
%\crefname{Lemma}{Lemma}{Lemmas} % let cleveref (\cref{}) refer to a label on the lemma environment as ``Lemma'' for 1 reference, or ``Lemmas'' for several.
%\newenvironment{lemma}
%{\begin{absolutelynopagebreak}
%		\begin{Lemma}}
%		{\end{Lemma}
%\end{absolutelynopagebreak}}
%
%\declaretheorem[sibling=Theorem, thmbox={style=S, bodystyle=\normalfont}, name=METHOD]{Method} % `sibling` ties the counter to the given theorem-like environment (in this case ``method'').
%\crefname{Method}{Method}{Methods} % let cleveref (\cref{}) refer to a label on the method environment as ``Method'' for 1 reference, or ``Methods'' for several.
%\newenvironment{method}
%{\begin{absolutelynopagebreak}
%		\begin{Method}}
%		{\end{Method}
%\end{absolutelynopagebreak}}
%\declaretheorem[sibling=Theorem, thmbox={style=S, bodystyle=\normalfont}, name=DEFINITION]{Definition} % `sibling` ties the counter to the given theorem-like environment (in this case ``definition'').
%\crefname{Definition}{Definition}{Definitions}% let cleveref (\cref{}) refer to a label on the definition environment as ``Definition' for 1 reference, or ``Definitions'' for several.
%\newenvironment{definition}
%{\begin{absolutelynopagebreak}
%		\begin{Definition}}
%		{\end{Definition}
%\end{absolutelynopagebreak}}
%\declaretheorem[sibling=Theorem, thmbox={style=S, bodystyle=\normalfont}, name=NOTATION]{Notation} % `sibling` ties the counter to the given theorem-like environment (in this case ``notation'').
%\crefname{Notation}{Notation}{Notations} % let cleveref (\cref{}) refer to a label on the notaion environment as ``Notation'' for 1 reference, or ``Notations'' for several.
%\newenvironment{notation}
%{\begin{absolutelynopagebreak}
%		\begin{Notation}}
%		{\end{Notation}
%\end{absolutelynopagebreak}}
%%%%%%%%%%
%% Environments %%
\NewEnviron{brace*}{%
\begin{equation*}
	\left\{ \begin{alignedat}{5}
		\BODY
	\end{alignedat} \right.
\end{equation*}
}
% Left brace containing multiple equations tied to one label
\NewEnviron{brace-one}[1]{%
\begin{equation} #1
	\left\{ \begin{alignedat}{5}
		\BODY
	\end{alignedat} \right.
\end{equation}
}
% Left brace containing multiple equation and each equation has its own label
\NewEnviron{brace-all}[1]{% 
	\begin{subequations} #1 \begin{empheq}[left=\empheqlbrace]{align}
			\BODY
	\end{empheq} \end{subequations}
}
% A split-up, long equation, with no label
\NewEnviron{spliteq*}{% 
	\begin{equation*} \begin{split}
			\BODY
	\end{split}	\end{equation*} 
}
% A split-up, long equation, with one label
\NewEnviron{spliteq}[1]{% 
	\begin{equation} #1 \begin{split}
			\BODY
	\end{split}	\end{equation} 
}


\newlist{steps}{enumerate}{1} % environment for proof steps, called ``steps''
\setlist[steps, 1]{wide, labelwidth=!, labelindent=0pt, label=Step \arabic*., font=\bfseries} % the steps environment has each item start with bold ``Step x.'' (but when referenced in the text, is non bold by setting font=\bfseries, rather than label=\textbf{Step \arabic*.}), and the following lines are not indented.
%%%%%%%%%%#1 \begin{cases}
% Referencing %
%%%%%%%%%%
\numberwithin{equation}{section} % Equations are labelled as (section number. equation number in section). `hypertexnames=false` flag is required in hyperref so that e.g references to equation 1b), and 2) do not point to the same equation; OR `naturalnames=true`.
%\counterwithin*{equation}{section} % Resets the equation counter for each section. Depends on package `chngcntr`.  `hypertexnames=false` flag is required in hyperref so that e.g references to equation 1b), and 2) do not point to the same equation. The `naturalnames=true` flag for hyperref fails because two equations could have number 2) in different sections, and the reference link would point to only the first occurence.
\crefformat{section}{\S#2#1#3} % Use the section symbol rather than the word ``Section'' to reference sections with \cref{}.
%%%%%%
% Index %
%%%%%%
\makeindex[intoc] % include index in the table of contents.
\usepackage{caption}
\captionsetup{font=small}
%\DeclareCaptionFont{10pt}{\fontsize{10pt}{10pt}\selectfont}
%\captionsetup{font=10pt}
\usepackage{setspace}
\onehalfspacing

\begin{document}
	\include{frontmatter}
	\include{introduction}
	\include{section2}
	\include{section3}
	\include{section4}
	\include{conclusion}
	\include{acknowledgements}
	\include{appendices}
	\include{backmatter}
\end{document}