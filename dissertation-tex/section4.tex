\section{Advanced theory and application}

The concepts discussed in the last two sections have been extended to systems in works by author \authnumcite{DiPernaMvs1985}, \authnumcite{BianchiniBressanVVS2005}, among others. An example of such a system is the Shallow Water Equations in 1 spatial dimension, also known as the Open Channel Flow equations (\authnumcite{Southard1It2006} - Course textbook $ \vert $ Chapter 5), utilised to model rivers, as seen in \authnumcite{ErsoyEtAlASV2021}. Built on that knowledge of systems are subsequent multi-dimensional systems of conservation laws, as done in works including those of  \authnumcite{LeVequeFVM2002}, \authnumcite{GuinotAat2005}, and \authnumcite{KruzkovFOQ1970}. \\

Understanding of the initial value problem for multi-dimensional systems of conservation laws enables an understanding of the Shallow Water Equations (in $ n $ spatial dimensions), and such knowledge in turn enables the construction of simulations built on this model. Thus far, we have discussed the initial value problem of scalar conservation laws in a theoretical manner. The introduction of computation requires numerical methods and discretisation, to avoid infinitely long runtimes. We discuss such changes, and give an example which includes some key mechanisms which we can link to our learning of the Riemann problem and solution. Firstly, due to the aforementioned theoretical continuity, a numerical discretisation method is required to transform the initial value problem into (discretised) Riemann problems (\authnumcite{LeVequeFVM2002} - \S 4). Included in the numerical discretisation method is a correction term, whose magnitude can be managed using a \dex{limiter} (\authnumcite{LeVequeFVM2002} - \S 6.3).  The Riemann problem is solved using a \dex{Riemann solver} (\authnumcite{LeVequeFVM2002} - \S 13.10, and \S 15.3). \\

As an example of how these concepts come together, we look to a tsunami simulation by \authnumcite{BrittonB2020}, in which the Shallow Water Equations were converted into Riemann problems using the Corner Transport Upwind method plus MC limiters. The homogeneous form of each Riemann problem was solved using Godunov's splitting method - which provided initial values for the inhomogeneous Riemann problem to be solved via the Lax-Wendroff method. %\\
The simulation in this example was developed to show the effects of friction on tsunami waves, and some of the results can be seen in \cref{fig:sim-disch-disp-wv}, and \cref{fig:sim-sse}. These figures illustrate the complex operations that can be performed building on the work we have done in the previous chapters. \\
\begin{figure}
	% 	\centering
	\makebox[\textwidth][c]{%
		\begin{tikzpicture}
			\node at (9,0) {};
			\node at (0,5.75) {\includegraphics[width=15cm,height=6cm]{buoy_123-gauge_discharge}};
			\node at (0,0) {\includegraphics[width=15cm,height=6cm]{Constitución_coast_(6km_west)-gauge_displacement}};
			\node at (0,-5.75) {\includegraphics[width=15cm,height=6cm]{Constitución_coast_(6km_west)-gauge_Wvelocity}};
		\end{tikzpicture}
	}
	\caption{Discharge, displacement, and wave velocity measured by buoys at different locations in the sea during a simulation of the Chile 2010 tsunami. Coloured lines distinguish simulations ran with a specific Manning's friction coefficient. From \authnumcite{BrittonB2020}.}
	\label{fig:sim-disch-disp-wv}
\end{figure}

\begin{figure}
	% 	\centering
\makebox[\textwidth][c]{%
		\begin{tikzpicture}
			\node at (2,-6) {\includegraphics[width=15cm,height=6cm]{buoy_32412-compare_gauge_height}};
		\end{tikzpicture}
	}
	\caption{Sea surface elevation measured by a buoy in the South Pacific Ocean during a simulation of the 2010 Chile tsunami. Coloured lines distinguish simulations ran with a specific Manning's friction coefficient. From \authnumcite{BrittonB2020}.}
		\label{fig:sim-sse}
\end{figure}

Hence we have discussed some sources by which to further build knowledge towards understanding the problem for the Shallow Water Equations, and introduced some key aspects of building a simulation on these equations - using the concepts derived in this document.