\section{Characteristics}

Before dealing with the specific case in hand, let us observe the IVP for the basic non-linear first-order PDE in $ n $ space dimensions: 
\begin{brace-all}{\label{eq:basic_pw}}
	F\Duux = 0 &\quad \text{in } U \subseteq \R^n \label{eq:basic_pw-pde} \\
	u = g &\quad \text{on } \Gamma \subseteq \partial U, \label{eq:basic_pw-boundary}
\end{brace-all}
where given functions $ \tfns{g}{\Gamma}{\R} $, and $ F $ are smooth.\\

Directly solving a PDE is often a greater challenge than solving an ordinary differential equation (\dex{ODE}). We thus aim to solve \eqref{eq:basic_pw} by converting it into a system of ODEs, and then applying relevant methods. This is the \dex{Method of Characteristics}. 

\subsection{The characteristic equations} \label{sec:find-char-ode}
Suppose the function $ u $ solves \eqref{eq:basic_pw}, and fix some $ \mx{x} \in U $ and $ \mx{x}^0 \in \Gamma $. Then $ u(\mx{x}^0) = g^0 $ is known from \eqref{eq:basic_pw-boundary}. Thus, we aim to calculate that $ u $ which describes the curve in $ U $ connecting $ \mx{x}^0 $ to $ \mx{x} $.
\begin{steps}
	\item \label{stp:str_chr_eq-setup}
	Let there be a curve in $ U $ which is defined by $ \mpf{x}(s) = \vect{\pf{x}_1 (s), \ldots, \pf{x}_n (s)} $, where $s \in I \subseteq \R $. Assume that $u \in C^2 (U) $ is a solution of \eqref{eq:basic_pw-pde}, and define the values of $ u $ along the curve:
	\begin{equation} \label{eq:z-def}
		\pf{z}(s) \coloneqq u(\mpf{x}(s)),
	\end{equation}
	and also the values of the gradient:
	\begin{equation*} 
		\mpf{p}(s) \coloneqq Du(\mpf{x}(s)),
	\end{equation*}
	where $\mpf{p}(s) = \vect{\pf{p} _1 (s), \ldots, \pf{p}_n (s)} $, for:
	\begin{equation} \label{eq:pi-def} 
		\pf{p}_i (s) \coloneqq \pdiff{\pf{x}_i} u(\mpf{x}(s)), \eqfor i = 1, \ldots, n.
	\end{equation}
	We need to choose $\mpf{x}(\cdot)$ such that $\pf{z}(\cdot)$ and $\mpf{p}(\cdot)$ are computable.
	We achieve this by assembling a system of ODEs.
	
	\item 
	First let us evaluate the PDE \eqref{eq:basic_pw-pde} at $ \mx{x} = \mpf{x}(s) $ so we get:
	\begin{equation} \label{eq:basic-eval-x}
		F\Duux = F\pszsxs = 0.
	\end{equation} 
	We differentiate this with respect to $ \pf{x}_i $:
	\begin{spliteq*}
		\pdiff{\pf{x}_i} F\pszsxs &= \sum_{j = 1}^{n} \pdiff{\pf{p}_j}F\pszsxs \cdot \pdiff{\pf{x}_i} \pf{p}_j(s) \\
		&\quad + \pdiff{\pf{z}}F\pszsxs \cdot \pdiff{\pf{x}_i} \pf{z}(s) \\
		&\quad + \pdiff{\pf{x}_i}F \pszsxs \\
		&= 0,
	\end{spliteq*}
	which is equivalent to:
	\begin{spliteq}{\label{eq:basic-x-diff-xi}}
		\pdiff{\pf{x}_i} F\pszsxs &=\sum_{j = 1}^{n} \pdiff{\pf{p}_j}F\pszsxs \cdot \pdiff{\pf{x}_i} \pdiff{\pf{x}_j} u(\mpf{x}(s)) \\
		&\quad + \pdiff{\pf{z}}F\pszsxs \cdot \pf{p}_i (\mpf{x}(s)) \\
		&\quad + \pdiff{\pf{x}_i}F \pszsxs \\
		&= 0.
	\end{spliteq}
	The second order derivative in this equation could increase the complexity of the task, so we aim to substitute it for a first order derivative. To do so, first calculate the differential of \eqref{eq:pi-def}, $\pf{p}_i(s) $:
	\begin{equation} \label{eq:pi-diff} 
		\dot{\pf{p}}_i (s) = \sum_{j=1}^{n} \pdiff{\pf{x}_j} \pdiff{\pf{x}_i} u (\mpf{x}(s)) \cdot \dot{\pf{x}}_j (s),
	\end{equation}
	where:
	\begin{equation*}
		\dot{} = \frac{d}{ds}.
	\end{equation*}
	Observe that equation \eqref{eq:pi-diff} contains a second derivative similar to that in \eqref{eq:basic-x-diff-xi}.
	Then let us assume that we may set:
	\begin{equation} \label{eq:xj-diff-F} 
		\dot{\pf{x}}_j (s) \coloneqq \pdiff{\pf{p}_j} F\pszsxs,
	\end{equation}
	and substitute this into \eqref{eq:pi-diff} for:
	\begin{equation*} %\label{eq:pi-diff-sum}
		\dot{\pf{p}}_i (s) = \sum_{j=1}^{n} \pdiff{\pf{x}_j} \pdiff{\pf{x}_i} u(\mpf{x}(s)) \cdot \pdiff{\pf{p}_j} F\pszsxs.
	\end{equation*}
	We can deploy the above equation into \eqref{eq:basic-x-diff-xi} as:
	\begin{equation} \label{eq:pi-diff-F}
		\dot{\pf{p}}_i (s) = - \pdiff{\pf{z}} F\pszsxs \cdot \pf{p}_i - \pdiff{\pf{x}_i} F \pszsxs,
	\end{equation}
	where the equivalence holds by:
	\begin{equation*}
		\pdiff{\pf{x}_i} \pdiff{\pf{x}_j} u(\mpf{x}(s)) = \pdiff{\pf{x}_j} \pdiff{\pf{x}_i} u(\mpf{x}(s)),
	\end{equation*}
	implied by \entref{apx:clairaut-thm}. 	
	Finally, we differentiate $ \pf{z}(s) $:
	\begin{equation} \label{eq:z-diff-F}
		\dot{\pf{z}}(s) = \sum_{j=1}^{n} \pdiff{\pf{x}_j} u(\mpf{x}(s)) \cdot \dot{\pf{x}}_j (s) 
		\nb{=}{\ast} \sum_{j=1}^{n} \pf{p}_j (s) \cdot \pdiff{\pf{p}_j} F \pszsxs,
	\end{equation}
	with the equality$ \nb{=}{\ast} $holding by \eqref{eq:pi-def} and \eqref{eq:xj-diff-F}.
	
	\item
	Thus we may assemble the aforementioned system of ODEs - named the \dex{characteristic equations} - by writing \eqref{eq:xj-diff-F}, \eqref{eq:pi-diff-F} and \eqref{eq:z-diff-F} in vector notation:
	\begin{brace-all} {\label{eq:char_ode}}
		\dot{\mpf{p}}(s) &= - D_\mpf{x} F\pszsxs - D_\pf{z} F\pszsxs \cdot \mpf{p}(s) 
		\label{eq:char_ode-p}\\
		\dot{\pf{z}}(s) &= D_\mpf{p} F\pszsxs \cdot \mpf{p}(s)
		\label{eq:char_ode-z} \\
		\dot{\mpf{x}}(s) &= D_\mpf{p} F\pszsxs,
		\label{eq:char_ode-x}
	\end{brace-all}
	and we restate from \eqref{eq:basic-eval-x}:
	\begin{equation} \label{eq:char_ode-F=0}
		F\pszsxs = 0.
	\end{equation}
	The above identities hold for $ s \in I $.	We will call $ \mpf{p}(\cdot) , \pf{z}(\cdot) $ the \dex{characteristics}, and call $ \mpf{x}(\cdot) $ the \dex{projected characteristic}, as it is the projection of $ \tuple{\mpf{p}(\cdot), \pf{z}(\cdot), \mpf{x}(\cdot)} \in \R^n \x \R \x \R^n $ onto $ U \subset \R^n $.
\end{steps}

This proves the following theorem:
\begin{theorem}[Structure of the characteristic equations] \label{thm:struct-char-ode}
	Let the solution of the non-linear, first-order PDE \eqref{eq:basic_pw-pde} be the function $ u \in C^2 (U) $. Assume $ \mpf{x}(\cdot) $ solves the ODE \eqref{eq:char_ode-x}, such that $ \mpf{p}(\cdot) = Du(\mpf{x}(\cdot)) \mcomma \text{and } \pf{z}(\cdot) = u(\mpf{x}(\cdot)) $. Then $ \mpf{p}(\cdot) $ solves the ODE \eqref{eq:char_ode-p}, and $ \pf{z}(\cdot) $ solves the ODE \eqref{eq:char_ode-z}, for $ s $ such that $ \mpf{x}(s) \in U $.
\end{theorem}

Thus, we have a system of ODEs \eqref{eq:char_ode} which can give us information about the solution of the IVP \eqref{eq:basic_pw}, however, we cannot find anything other than a general solution of the characteristic equations without appropriate initial conditions. 

\subsection{Boundary conditions}

We now set out to discover those initial conditions of the characteristic equations \eqref{eq:char_ode}, whose solution will provide information about the IVP \eqref{eq:basic_pw}.

\subsubsection{Straightening the boundary}
To simplify the following calculations, we change variables to ``flatten out'' part of the boundary $ \partial U $. Firstly, we fix $ \mx{x}^0 \in \partial U $. We then find mappings $ \tfns{\mx{\Phi}, \mx{\Psi}}{\R^n}{\R^n} $ for which $ \mx{\Psi} = \mx{\Phi}^{-1} $, and $ \mx{\Phi} $ straightens the boundary $ \partial U $ near $ \mx{x}^0 $ (see \cref{apx:straight-bound}).\\

Given any function $ \tfns{u}{U}{\R} $, we define $ V \coloneqq \mx{\Phi}(U) $ and let:
\begin{equation*}
	v(\mx{y}) \coloneqq u(\mx{\Psi}(\mx{y})), \eqfor \mx{y} \in V.
\end{equation*}
Then, using:
\begin{brace*} 
	\mx{y} &= \mx{\Phi}(\mx{x})\\
	\mx{x} &= \mx{\Psi}(\mx{y}),
\end{brace*}
we resolve that:
\begin{equation} \label{eq:u_ito_v}
	u(\mx{x}) = v(\mx{\Phi}(\mx{x})), \eqfor \mx{x} \in U.
\end{equation}
Supposing $ u \in C^1 (U) $ is a solution of the IVP \eqref{eq:basic_pw}, we now aim to identify the PDE which $ v \in V $ is a solution of. Firstly, we observe that \eqref{eq:u_ito_v} implies:
\begin{spliteq*}
	\pdiff{x_i} u (\mx{x}) &= \sum_{k=1}^{n} \pdiff{\Phi_{k} (\mx{x})} v(\mx{\Phi}(\mx{x})) \cdot \pdiff{x_i} \Phi_{k} (\mx{x}) , \eqfor i = 1,\ldots,n \\
	&\equiv \sum_{k=1}^{n} \pdiff{y_k} v(\mx{\Phi}(\mx{x})) \cdot \pdiff{x_i} \Phi_{k} (\mx{x}).\\
\end{spliteq*}
We can re-write this in vector notation as:
\begin{equation*}
	Du(\mx{x}) = Dv(\mx{y}) \cdot D\mx{\Phi}(\mx{x}).
\end{equation*}
Thus, gathering all of the equations in this subsection together, the PDE \eqref{eq:basic_pw-pde} implies:
\begin{equation*}
	F(Du(\mx{x}), u(\mx{x}), \mx{x}) = F(Dv(\mx{y}) \cdot D\mx{\Phi}(\mx{\Psi}(\mx{y})), v(\mx{y}), \mx{\Psi}(\mx{y})) = 0,
\end{equation*}
which we can express more simply as:
\begin{equation} \label{eq:G_ito_y}
	G(Dv(\mx{y}), v(\mx{y}), \mx{y}) = 0.
\end{equation}
Now, let us denote:
\begin{brace*}
	\Delta &\coloneqq \mx{\Phi}(\Gamma) \\
	h(\mx{y}) &\coloneqq g(\mx{\Psi}(\mx{y})).
\end{brace*}
Using this and \eqref{eq:G_ito_y}, the analogue to the problem \eqref{eq:basic_pw} is thus:
\begin{brace*}
	G(Dv, v, \mx{y}) &= 0 \quad \text{in } V \\
	v &= h \quad \text{on } \Delta.
\end{brace*}
Observe from the above, that the transformation of \eqref{eq:basic_pw} to a problem with a straightened out boundary near $ \mx{x}^0 $, maintains the form of the original problem.

\subsubsection{Compatibility conditions}
From the previous findings - and given $ \mx{x}^0 \in \Gamma \subseteq \partial U $, we may assume the following: $ \Gamma $ is flat near $ \mx{x}^0 $, in the plane $ \Set{x_n = 0} $. To construct a solution to the IVP \eqref{eq:basic_pw} using the characteristic equations \eqref{eq:char_ode} near $ \mx{x} = \mx{x}^0 $, we require appropriate initial ($ s = 0 $) conditions for the characteristic equations as follows:
\begin{brace-one}{\label{eq:ini_cond-0}}
	\mpf{x}(0) &= \mx{x}^0 \\
	\mpf{p}(0) &= Du(\mx{x}^0) = \mx{p}^0 \\
	\pf{z}(0) &= u(\mx{x}^0) = z^0.
\end{brace-one}

The curve $ \mpf{x}(\cdot) $ passes through the point $ \mx{x}^0 $, which by definition means that:
\begin{equation} \label{eq:z0-eq}
	z^0 = u(\mx{x}^0) = g(\mx{x}^0).
\end{equation}
And furthermore, the implication of the initial condition \eqref{eq:basic_pw-boundary} of the PDE is that near $ \mx{x}^0 $ (and hence in the plane $ \Set{x_n = 0} $), we have:
\begin{equation*}
	u(x_1, \ldots, x_n) = u(x_1, \ldots, x_{n-1}, 0) = u(x_1, \ldots, x_{n-1}) = g(x_1, \ldots, x_{n-1}),
\end{equation*}
which gives the differential equation:
\begin{equation*}
	\pdiff{x_i} u(\mx{x}^0) = \pdiff{x_i} g(\mx{x}^0) , \eqfor i = 1, \ldots, n-1.
\end{equation*}
Thus by definition, we should have:
\begin{equation} \label{eq:p0-eq}
	p^{0}_i = \pdiff{x_i} g (\mx{x}^0), \eqfor i = 1, \ldots, n-1,
\end{equation}
where $ \mx{p}^0 = \vect{p^{0}_1, \ldots, p^{0}_n} $. 
And for the PDE \eqref{eq:basic_pw-pde} to hold, we also require:
\begin{equation} \label{eq:F0-eq}
	F( \pozoxo ) = 0.
\end{equation}
Thus we collect \eqref{eq:z0-eq}, \eqref{eq:p0-eq}, \eqref{eq:F0-eq} as:
\begin{brace-all}{\label{eq:cmpt_cndts}}
	z^0 &= g(\mx{x}^0) \label{eq:cmpt_cndts-z0-eq}\\
	p^{0}_i &= \pdiff{x_i} g (\mx{x}^0) , \eqfor i = 1, \ldots, n-1 \label{eq:cmpt_cndts-p0-eq}\\ 
	F( \pozoxo ) &= 0. \label{eq:cmpt_cndts-F0-eq}
\end{brace-all}
These equations are called the \dex{compatibility conditions}. The triple $ \tuple{\pozoxo} \in \R^n \x \R^n \x \R $ which satisfies \eqref{eq:cmpt_cndts} is called \dex{admissible}. 

\subsubsection{Non-characteristic boundary data}
As done before, let us set $ \mx{x}^0 \in \Gamma $, and assume the plane $ \Set{x_n = 0} $ contains the points of $ \Gamma $ near $ \mx{x}^0 $. Also assume that the triple $ \tuple{\pozoxo} $ is admissible. Then we aim to find a solution $ u $ of \eqref{eq:basic_pw} in $ U $ near $ \mx{x}^0 $, by integrating the characteristic equations \eqref{eq:char_ode}. %\\
We understand that the admissible triple are suitable boundary conditions of the characteristic equations, with the line $ \mpf{x}(\cdot) $ intersecting the boundary $ \Gamma $ at the point $ \mx{x}^0 $ as alluded to in \stpprfref{stp:str_chr_eq-setup}{thm:struct-char-ode}. %\\
To achieve our goal we look to expand the solution of the characteristic equations to points near $ \mx{x}^0 $, by introducing some perturbation into the admissible triple in such a way that the compatibility conditions still hold.\\

For a given point $ \mx{y} = \vect{y_1, \ldots, y_{n-1}, y_n = 0} \in \Gamma $ close to $ \mx{x}^0 $, we therefore aim to solve the characteristic equations \eqref{eq:char_ode} with the initial conditions:
\begin{brace-one}{\label{eq:ini_cond-y}}
	\mpf{x}(0) &= \mx{y} \\
	\mpf{p}(0) &= Du(\mx{y}) = \mx{q}(\mx{y}) \\
	\pf{z}(0) &= g(\mx{y}).		
\end{brace-one}

\begin{steps}
	\item
	Clearly we have an unknown function $ \mx{q}(\cdot) $, which will satisfy the initial conditions \eqref{eq:ini_cond-0} of the characteristic equations, if:
	\begin{equation}
		\mx{q}(\mx{x}^0) = Du(\mx{x}^0) = \mx{p}^0.
		\label{eq:q_ito_p0}
	\end{equation}
	And thus, an admissible triple $ \qgy $ must satisfy the compatibility conditions \eqref{eq:cmpt_cndts}, that is:
	\begin{brace-one}{\label{eq:y_cmpt_cndts}}
		g(\mx{y}) &= g(\mx{y}) \\
		q_i &= \pdiff{x_i} g (\mx{y}) , \eqfor i = 1, \ldots, n-1 \\ 
		F(\mx{q}(\mx{y}), g(\mx{y}), \mx{y}) &= 0,
	\end{brace-one}
	for all $ \mx{y} \in \Gamma $ near $ \mx{x}^0 $. %\\
	Hence, we search for $ \mx{q}(\cdot) $ such that \eqref{eq:q_ito_p0}, \eqref{eq:y_cmpt_cndts} hold. %\\ 
	Since we know $ q_i $ for $ i = 1, \ldots, n-1 $; we need only find $ q_n(\mx{y}) $ for which:
	\begin{brace*}
		F\qgy &= 0\\
		q_n (\mx{x}^0) &= p^0_n.
	\end{brace*}

	\item
	Recall that in the IVP \eqref{eq:basic_pw}, it is assumed that $ F $ is smooth on $ U $, which implies $ F \in C^1 (U) $. Also recall from \eqref{eq:cmpt_cndts-F0-eq} that $ F( \pozoxo ) = 0 $. Therefore, if we require that:
	\begin{equation*}
		\pdiff{\pf{p}_n} F( \pozoxo ) \neq 0,
	\end{equation*}
	then by \entref{apx:imp-fun-thm}, there exists a function $ \Upsilon $ such that $ \Upsilon (p^{0}_{1}, \ldots, p^{0}_{n-1}, z^0, \mx{x}^0) = p^{0}_{n} $. In general, we have a unique, local solution $ q_n(\mx{y}) = \Upsilon (q_{1} (\mx{y}), \ldots, \pf{q}_{n-1} (\mx{y})), g (\mx{y}), \mx{y}) $.
\end{steps}

From this finding, we have proven the following lemma:
\begin{lemma}[Non-characteristic boundary conditions] \label{lem:non-chr_bnd_cnd}
	There exists a unique solution $ \mx{q}(\cdot) $ of \eqref{eq:q_ito_p0}, \eqref{eq:y_cmpt_cndts} for all $ y \in \Gamma $ near enough to $ \mx{x}^0 $, provided that:
	\begin{equation} \label{eq:non-chr_cond}
		\pdiff{\pf{p}_n} F( \pozoxo ) \neq 0.
	\end{equation}
\end{lemma}

We call the admissible triple $ ( \pozoxo ) $ \dex{non-characteristic} if \eqref{eq:non-chr_cond} holds, and we assume this condition does indeed hold moving forward.

\subsection{Local solution}
Recall that our objective is to find $ u $ which solves the IVP \eqref{eq:basic_pw} - at least near $ \Gamma $ - using the characteristic equations \eqref{eq:char_ode}. %\\
As done previously, we fix $ \mx{x}^0 \in \Gamma $, assume $ \Gamma $ is flat near $ \mx{x}^0 $ and lies in the plane $ \Set{ x_n = 0 } $. Furthermore, we assume that $ ( \pozoxo ) $ is a non-characteristic, admissible triple. These conditions enable the application of \entref{lem:non-chr_bnd_cnd}, which states that there exists a function $ \mx{q}(\cdot) $ such that $ \mx{p}^0 = \mx{q}(\mx{x}^0) $ and the triple $ \qgy $ is admissible, where $ \mx{y} $ is near enough to $ \mx{x}^0 $. Indeed, given such a point $ \mx{y} = \vect{y_1, \ldots, y_{n-1}, y_n = 0} $, we solve the characteristic equations \eqref{eq:char_ode}, subject to the initial conditions \eqref{eq:ini_cond-y}.\\

\begin{notation} \label{not:dep-sol-chr_eq}
	We represent the dependence of the solution of the IVP of the characteristic equations \eqref{eq:char_ode}, \eqref{eq:ini_cond-y}, on $ s $ and $ \mx{y} $ as:
	\begin{brace*}
		\mpf{p}(s) &= \mpf{p}(\mx{y},s) = \mpf{p}(y_1, \ldots, y_{n-1}, s) \\
		\pf{z}(s) &= \pf{z}(\mx{y},s) = \pf{z}(y_1, \ldots, y_{n-1}, s) \\
		\mpf{x}(s) &= \mpf{x}(\mx{y},s) = \mpf{x}(y_1, \ldots, y_{n-1}, s).
	\end{brace*}
	Hereafter, we also identify $ \mx{x}^0 $ as lying in the plane $ \R^{n-1} $.
\end{notation}

We now attempt to show that the function $ \tfns{\mpf{x}}{(\mx{y},s)}{\mx{x}} $ is invertible near $ \mx{x}^0 $ in $ \Gamma \subset \R^{n-1} $, which we can achieve using the \entref{apx:inv-fun-thm}.

\begin{steps}
	\item
	Suppose that there exists: an open interval $ I \subseteq \R $, with $ 0 \in I $; a neighbourhood $ V \subseteq \R^n $ of $ \mx{x}^0 $; and a neighbourhood $ W \subseteq \Gamma \subset \R^{n-1} $ of $ \mx{x}^0 $. Then for each $ \mx{x} \in V $, there exists $ s \in I \mcomma \mx{y} \in W $ such that $ \mx{x} = \mpf{x}(\mx{y}, s) $. %\\
	If $ \mpf{x} \in C^1(W \x I) $, and $ \det D \mpf{x}^0 \neq 0 $, then the \entref{apx:inv-fun-thm} implies the function $ \mpf{x} $ is invertible on $ V $.
	
	\item
	$ \mpf{x} $ is a construct, thus we may simply let $ \mpf{x} \in C^1(W \x I) $. 
	\item
	Indeed we can show $ \det D \mpf{x}^0 \neq 0 $ as follows. %\\
	By \eqref{eq:ini_cond-y}, we have:
	\begin{equation*}
		\mpf{x}(\mx{y},0) = \vect{y_1, \ldots, y_{n-1}, 0} = \mx{y} , \eqfor \mx{y} \in \Gamma,
	\end{equation*}
	therefore trivially showing that $ \mpf{x}(\mx{x}^0, 0) = \mx{x}^0 $. And so, for $ i = 1, \ldots, n $, we obtain $ D \mpf{x}^0 $ in element form as:
	\begin{equation*}
		\pdiff{y_i} \pf{x}_j (\mx{x}^0, 0) = \pdiff{y_i} x^{0}_{j} = \begin{cases}
			\delta_{ij}, &\eqfor j = 1, \ldots, n-1 \\
			0, &\eqfor j = n.
		\end{cases}
	\end{equation*}
	Also, from the characteristic equation \eqref{eq:char_ode-x} it is implied that:
	\begin{equation*}
		\dot{\pf{x}}_j (s=0) = \pdiff{s} \pf{x}_j (\mx{x}^0,0) = \pdiff{\pf{p}_j} F( \pozoxo ).
	\end{equation*}
	Hence:
	\begin{equation*}
		D\mpf{x}(\mx{x}^0, 0) = \begin{pmatrix}
			1 &0 &\cdots &\pdiff{\pf{p}_1} F( \pozoxo ) \\
			&\ddots &&\vdots \\
			0 &\cdots &1 &\vdots \\
			0 &\cdots &\cdots &\pdiff{\pf{p}_n} F( \pozoxo ) 
		\end{pmatrix}_{n \x n} ,	
	\end{equation*}
	with $ \det D\mpf{x}(\mx{x}^0, 0) \neq 0 $ from the non-characteristic condition \eqref{eq:non-chr_cond}.
\end{steps}

Thus we have proved the following theorem:
\begin{lemma}[Local invertibility] \label{lem:loc_inv}
	Let the non-characteristic condition $ \pdiff{\pf{p}_n} F( \pozoxo ) \neq 0 $ hold. Then there exists: an open interval $ I \subseteq \R $, with $ 0 \in I $; a neighbourhood $ V \subseteq \R^n $ of $ \mx{x}^0 $; and a neighbourhood $ W \subseteq \Gamma \subset \R^{n-1} $ of $ \mx{x}^0 $. Then for each $ \mx{x} \in V $, there exists a unique $ s \in I \mcomma \mx{y} \in W $ such that:
	\begin{equation*}
		\mx{x} = \mpf{x}(\mx{y}, s).
	\end{equation*}
	The function $ \mpf{x} $ is $ C^2 $ also.
\end{lemma}

From \entref{lem:loc_inv}, we can locally uniquely solve:
\begin{equation}{\label{eq:loc_uni_sol-x}}
	\mx{x} = \mpf{x}(\mx{y}, s), \eqfor \mx{y} = \mpf{y}(\mx{x}) \mcomma \text{and } s = \pf{s}(\mx{x}),
\end{equation}
for each $ \mx{x} \in V $.
Similarly, for $ \mx{x} \in V $ and $ \mx{y}, s $ as in \eqref{eq:loc_uni_sol-x}, we define:
\begin{brace-one}{\label{eq:loc_uni_sol-u,p}}
	u(\mx{x}) &\coloneqq \pf{z}(\mpf{y}(\mx{x}), \pf{s}(\mx{x})) \\
	\mpf{p}(\mx{x}) &\coloneqq \mpf{p}(\mpf{y}(\mx{x}), \pf{s}(\mx{x})).
\end{brace-one}

Finally, we attempt to weave these solutions of the characteristic equations \eqref{eq:char_ode} together to solve the IVP \eqref{eq:basic_pw}.

\begin{steps}
	\item We begin by fixing $ \mx{y} \in \Gamma $ near $ \mx{x}^0 $. As before, we solve the characteristic equations \eqref{eq:char_ode} with initial conditions \eqref{eq:ini_cond-y}, for $ \mpf{p}(s) = \mpf{p}(\mx{y}, s) $, $ \pf{z}(s) = \pf{z}(\mx{y}, s) $, and $ \mpf{x}(s) = \mpf{x}(\mx{y}, s) $, just like in \cref{not:dep-sol-chr_eq}.
	
	\item We define the function:
	\begin{equation} \label{eq:def-f(y,s)}
		f(\mx{y}, s) \coloneqq F(\mpf{p}(\mx{y}, s), \pf{z}(\mx{y}, s), \mpf{x}(\mx{y}, s)).
	\end{equation}
	By the compatibility conditions \eqref{eq:y_cmpt_cndts} corresponding to the initial conditions \eqref{eq:ini_cond-y}, we know:
	\begin{equation} \label{eq:f-y,0}
		f(\mx{y}, 0) = F(\mpf{p}(\mx{y}, 0), \pf{z}(\mx{y}, 0), \mpf{x}(\mx{y}, 0)) = F\qgy = 0.
	\end{equation}
	Moreover, the differential of $ f $ \eqref{eq:def-f(y,s)} with respect to $ s $ is:
	\begin{spliteq*}
		\pdiff{s} f(\mx{y},s) &= \sum_{j=1}^{n} \pdiff{\pf{p}_j} F \cdot \dot{\pf{p}}_j 
		+ \pdiff{\pf{z} } F \cdot \dot{\pf{z}} 
		+ \sum_{j=1}^{n} \pdiff{\pf{x} _j} F \cdot \dot{\pf{x}}_j \\
		&\nb{=}{\ast} \sum_{j=1}^{n} \pdiff{\pf{p}_j} F \cdot (- \pdiff{\pf{x}_j} F - \pdiff{\pf{z}} F \cdot \pf{p}_j) \\ 
		&\quad + \sum_{j=1}^{n} \pdiff{\pf{z}} F \cdot (\pdiff{\pf{p}_j} F \cdot \pf{p}_j) 
		+ \sum_{j=1}^{n} \pdiff{\pf{x} _j} F \cdot (\pdiff{\pf{p}_j} F) \\
		&\equiv \sum_{j=1}^{n} \pdiff{\pf{p}_j} F \cdot ( \pdiff{\pf{x}_j} F - \pdiff{\pf{x}_j} F ) \\
		&\quad + \sum_{j=1}^{n} \pdiff{\pf{p}_j} F \cdot ( \pdiff{\pf{z}} F \cdot \pf{p}_j - \pdiff{\pf{z}} F \cdot \pf{p}_j ) \\
		&= 0.
	\end{spliteq*}
	The equality$ \nb{=}{\ast} $holds by the characteristic equations \eqref{eq:char_ode}. We summarise this calculation and \eqref{eq:f-y,0} as:
	\begin{brace*}
		\pdiff{s} f(\mx{y},s) &= 0 \\
		f(\mx{y},0) &= 0,
	\end{brace*}
	which implies $	f(\mx{y}, s) $ is constant in $ s $, and therefore:
	\begin{equation} \label{eq:f(y,s)=0}
		f(\mx{y}, s) = f(\mx{y}) \equiv f(\mx{y}, 0) = 0, \eqfor s \in I, 
	\end{equation}
	with $ \mx{y} \in \Gamma $ sufficiently close to $ \mx{x}^0 $.
	
	\item In light of \entref{lem:loc_inv}, we obtain:
	\begin{spliteq*}
		F(\mpf{p}(\mx{x}), u(\mx{x}), \mx{x}) &
		\nb{=}{\ast} F(\mpf{p}(\mx{y}, s), \pf{z}(\mx{y}, s), \mpf{x}(\mx{y}, s)) \\
		&\equiv f(\mx{y}, s) \nb{=}{\dagger} 0, \eqfor \mx{x} \in V.
	\end{spliteq*}
	where the equality$ \nb{=}{\ast} $holds by \eqref{eq:loc_uni_sol-x}, \eqref{eq:loc_uni_sol-u,p} , and$ \nb{=}{\dagger} $is given by \eqref{eq:f(y,s)=0}.
	To equalise the PDE \eqref{eq:basic_pw-pde} with \eqref{eq:char_ode-F=0}, it therefore remains to show that:
	\begin{equation} \label{eq:p(x)=Du(x)-to_show}
		\mpf{p}(\mx{x}) = Du(\mx{x}), \eqfor \mx{x} \in V.
	\end{equation}
	To achieve this, we first establish a new function:
	\begin{equation*}
		\pf{r}_i (s) \coloneqq \pdiff{y_i} \pf{z} (\mx{y},s) - \sum_{j=1}^{n} \pf{p}_j (\mx{y},s) \cdot \pdiff{y_i} \pf{x}_j (\mx{y},s),
	\end{equation*}
	For some fixed $ y \in \Gamma $, and $ i \in \Set{1, \ldots, n-1} $. Observe that the compatibility conditions \eqref{eq:y_cmpt_cndts} implies:
	\begin{spliteq*}
		\pf{r}_i (0) &= \pdiff{y_i} \pf{z} (\mx{y},0) - \sum_{j=1}^{n} \pf{p}_j (\mx{y},0) \cdot \pdiff{y_i} \pf{x}_j (\mx{y},0) \\
		&= \pdiff{y_i} g (\mx{y}) - \sum_{j=1}^{n} \pf{q}_j (\mx{y}) \cdot \pdiff{y_i} y_j \\
		&\nb{=}{\ast} \pdiff{y_i} g (\mx{y}) - \pf{q}_i (\mx{y}) = 0,
	\end{spliteq*}
	where the equality$ \nb{=}{\ast} $holds by $ \pdiff{y_i} y_j = \delta_{ij} $.
	We can also compute the derivative:
	\begin{equation} \label{eq:dot-r_i}
		\dot{\pf{r}}_i (s) \coloneqq \pdiff{s} \pdiff{y_{i}}  \pf{z} - \sum_{j=1}^{n} \left( \pdiff{s} \pf{p}_j\cdot \pdiff{y_i} \pf{x}_j + \pf{p}_j \cdot \pdiff{s} \pdiff{y_i} \pf{x}_j \right).
	\end{equation}
	We can simplify this equation by first recalling the derivative of $ \pf{z} $ given in \eqref{eq:z-diff-F}:
	\begin{equation} \label{eq:z-diff-wrt-s}
		\pdiff{s} \pf{z} =\sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{s} \pf{x}_j,
	\end{equation}
	and then differentiating with respect to $ y_i $, we obtain:
	\begin{equation*}
		\pdiff{y_i} \pdiff{s} \pf{z} = 
		\sum_{j=1}^{n} \left( \pdiff{y_i} \pf{p}_j\cdot \pdiff{s} \pf{x}_j
		+ \pf{p}_j\cdot \pdiff{y_i} \pdiff{s} \pf{x}_j \right),
	\end{equation*}
	which is equivalent to:
	\begin{equation*}
		\pdiff{s} \pdiff{y_i} \pf{z} = 
		\sum_{j=1}^{n} \left( \pdiff{y_i} \pf{p}_j\cdot \pdiff{s} \pf{x}_j
		+ \pf{p}_j\cdot \pdiff{s} \pdiff{y_i} \pf{x}_j \right),
	\end{equation*}
	 by $ \mpf{x} \in C^2 $ resulting from \entref{lem:loc_inv}, which enables the application of \entref{apx:clairaut-thm}.
	Now we can substitute this into \eqref{eq:dot-r_i}, to receive:
	\begin{spliteq}{\label{eq:dot-r_i-sub}}
		\dot{\pf{r}}_i (s) &= \sum_{j=1}^{n} \left( \pdiff{y_i} \pf{p}_j\cdot \pdiff{s} \pf{x}_j + \pf{p}_j\cdot \pdiff{s} \pdiff{y_i} \pf{x}_j \right) \\
		&\quad - \sum_{j=1}^{n} \left( \pdiff{s} \pf{p}_j\cdot \pdiff{y_i} \pf{x}_j + \pf{p}_j \cdot \pdiff{s} \pdiff{y_i} \pf{x}_j \right) \\
		& = \sum_{j=1}^{n} \left( \pdiff{y_i} \pf{p}_j\cdot \pdiff{s} \pf{x}_j - \pdiff{s} \pf{p}_j\cdot \pdiff{y_i} \pf{x}_j \right) \\
		&\nb{=}{\ast} \sum_{j=1}^{n} \left( \pdiff{y_i} \pf{p}_j\cdot \pdiff{\pf{p}_j} F - \left( - \pdiff{\pf{x}_j} F - \pdiff{\pf{z}} F \cdot \pf{p}_j \right) \cdot \pdiff{y_i} \pf{x}_j \right) \\
		&\equiv \sum_{j=1}^{n} \pdiff{y_i} \pf{p}_j\cdot \pdiff{\pf{p}_j} F + \sum_{j=1}^{n} \pdiff{\pf{x}_j} F \cdot \pdiff{y_i} \pf{x}_j + \sum_{j=1}^{n} \pdiff{\pf{z}} F \cdot \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j,
	\end{spliteq}
	where the equality$ \nb{=}{\ast} $is given by the characteristic equations \eqref{eq:char_ode-p}, \eqref{eq:char_ode-x}. We can simplify this expression by first taking the derivative of \eqref{eq:def-f(y,s)} with respect to $ y_i $:
	\begin{equation}
		\pdiff{y_i} f(\mx{y}, s) = \sum_{j=1}^n \pdiff{\pf{p}_j} F \cdot \pdiff{y_i} \pf{p}_j + \pdiff{\pf{z}} F \cdot \pdiff{y_i} \pf{z} + \sum_{j=1}^n \pdiff{\pf{x}_j} F \cdot \pdiff{y_i} \pf{x}_j = 0,
	\end{equation}
	and then combining this with \eqref{eq:dot-r_i-sub} to obtain:
	\begin{spliteq}{\label{eq:r_i-ode}}
		\dot{\pf{r}}_i (s) &= \pdiff{y_i} f(\mx{y}, s) - \pdiff{\pf{z}} F \cdot \pdiff{y_i} \pf{z} + \sum_{j=1}^{n} \pdiff{\pf{z}} F \cdot \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j \\
		&\nb{=}{\ast} \pdiff{\pf{z}} F \cdot \left( - \pdiff{y_i} \pf{z} + \sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j \right) \\
		&= - \pdiff{\pf{z}} F \cdot \pf{r}_i (s),
	\end{spliteq}
	where the equality$ \nb{=}{\ast} $is given by the identity \eqref{eq:f(y,s)=0}.
	Thus we have shown that $ \pf{r}_i (\cdot) $ solves the IVP:
	\begin{brace*}
		\dot{\pf{r}}_i (s) &= - \pdiff{\pf{z}} F \cdot \pf{r}_i (s) \\
		\pf{r}_i (0) &= 0.
	\end{brace*}
	Since the above ODE is linear, we can solve as follows using \entref{apx:int-fct-mth}:
	\begin{spliteq*}
		\frac{d \pf{r}_i}{d s} + \left( \pdiff{\pf{z}} F \cdot \pf{r}_i \right) = 0
		\implies &\exp \left(\int \pdiff{\pf{z}} F ds \right) \cdot \frac{d \pf{r}_i}{d s} + \exp \left(\int \pdiff{\pf{z}} F ds \right) \cdot \left( \pdiff{\pf{z}} F \cdot \pf{r}_i \right) = 0 \\
		\iff &\frac{d}{d s} \left(\exp \left(\int \pdiff{\pf{z}} F ds \right) \cdot \pf{r}_i \right) = 0 \\
		\iff &\exp \left(\int \pdiff{\pf{z}} F ds \right) \cdot \pf{r}_i = c, \eqfor c \in \R\\
		\iff &\pf{r}_i = \frac{c}{\exp \left(\int \pdiff{\pf{z}} F ds \right)}. \\
		\pf{r}_i (0) = 0 \iff &c = 0 \\
		\iff &\pf{r}_i = 0.
	\end{spliteq*}
	Thus we have shown $ \pf{r}_i (s) = 0 \mcomma \text{for } s \in I, \text{and } i = 1, \ldots, n - 1 $. Using this finding, we can also extract the following identity from \eqref{eq:r_i-ode}:
	\begin{spliteq}{\label{eq:z-diff-wrt-y_i}}
		&\pdiff{\pf{z}} F \cdot \left( - \pdiff{y_i} \pf{z} + \sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j \right) 
		= - \pdiff{\pf{z}} F \cdot \pf{r}_i (s) \\
		&\iff \pdiff{\pf{z}} F \cdot \left(  - \pdiff{y_i} \pf{z} + \sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j \right) = 0 \\
		&\nb{\iff}{\ast} - \pdiff{y_i} \pf{z} + \sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j = 0 \\
		&\iff \pdiff{y_i} \pf{z} = \sum_{j=1}^{n} \pf{p}_j \cdot \pdiff{y_i} \pf{x}_j,
	\end{spliteq}
	where the implication$ \nb{\iff}{\ast} $is given by $ F $ being smooth - as declared in \eqref{eq:basic_pw} - which implies $ \pdiff{\pf{z}} F \neq 0 $ all the time. 
		
	\item We can now employ our findings to show \eqref{eq:p(x)=Du(x)-to_show} indeed does hold. First - recalling the definitions \eqref{eq:loc_uni_sol-u,p} - we take an arbitrary element of $ Du $, that is the derivative of $ u $ with respect to $ \pf{x}_j \mcomma \text{for } j = 1, \ldots, n $:
	\begin{spliteq*}
		\pdiff{\pf{x}_j} u = \pdiff{s} \pf{z} \cdot \pdiff{\pf{x}_j} \pf{s} + \sum_{i=1}^{n-1} \pdiff{y_i} \pf{z} \cdot \pdiff{\pf{x}_j} \pf{y}_i,
	\end{spliteq*}
	then we use \eqref{eq:z-diff-wrt-s}, \eqref{eq:z-diff-wrt-y_i} to refine the equation as follows:
	\begin{spliteq*}
		&\pdiff{s} \pf{z} \cdot \pdiff{\pf{x}_j} \pf{s} + \sum_{i=1}^{n-1} \pdiff{y_i} \pf{z} \cdot \pdiff{\pf{x}_j} \pf{y}_i \\
		&= \left( \sum_{k=1}^{n} \pf{p}_k \cdot \pdiff{s} \pf{x}_k \right) \cdot \pdiff{\pf{x}_j} \pf{s} 
		+ \sum_{i=1}^{n-1} \left( \sum_{k=1}^{n} \pf{p}_k \cdot \pdiff{y_i} \pf{x}_k \right) \cdot \pdiff{\pf{x}_j} \pf{y}_i \\
		&= \sum_{k=1}^{n} \pf{p}_k \cdot \left( \pdiff{s} \pf{x}_k \cdot \pdiff{\pf{x}_j} \pf{s} 
		+ \sum_{i=1}^{n-1} \pdiff{y_i} \pf{x}_k \cdot \pdiff{\pf{x}_j} \pf{y}_i \right) \\
		&= \sum_{k=1}^{n} \pf{p}_k \cdot \pdiff{\pf{x}_j} \pf{x}_k 
		\nb{=}{\ast} \pf{p}_j,
	\end{spliteq*}
	with the equality$ \nb{=}{\dagger} $holding by $ \pdiff{\pf{x}_j} \pf{x}_k = \delta_{jk} $. To summarise the two equations above, we have:
	\begin{equation*}
		\pdiff{\pf{x}_j} u 	= \pf{p}_j,
	\end{equation*} 
	which proves \eqref{eq:p(x)=Du(x)-to_show}.
\end{steps}

Hence, we have carefully proven the following theorem:

\begin{theorem}[Local existence theorem] \label{thm:loc-exist-thm}
	The function $ u \in C^2 $ defined in \eqref{eq:loc_uni_sol-u,p} solves the IVP:
	\begin{brace*}
		F\Duux &= 0, &&\eqfor \mx{x} \in V \\
		u(\mx{x}) &= g(\mx{x}), &&\eqfor \mx{x} \in \Gamma \cap V.
	\end{brace*}
\end{theorem}

Therefore, we have derived criteria for a function $ u $ to be a solution to the initial value problem of the basic, non-linear partial differential equation in $ n $ space dimensions \eqref{eq:basic_pw}.